// This also counts as project 3 since I already simplified it :)
#include <stdio.h>

#define MSG_LENGTH 100

int main(void){
  char message[MSG_LENGTH], ch, *p;

  printf("Enter a message: ");
  for(p = message; p < message + MSG_LENGTH && (ch = getchar()) != '\n'; p++)
    *p = ch;

  printf("Reversal is: ");
  for(p--; p >= message; p--)
    putchar(*p);
  putchar('\n');

  return 0;
}
