/* This program reverses the words in a sentence. */
#include <stdio.h>

int main(void){
  int length;
  printf("Maximum number of characters? ");
  scanf("%d", &length);
  char ch = getchar(), sentence[length], term_char, *p, *p2;

  printf("Enter a sentence: ");
  for(p = sentence, ch = getchar();
  (ch != '.') && (ch != '?') && (ch != '!') && (ch != '\n');
  p++, ch = getchar())
    *p = ch;
  length = p - sentence;
  term_char = ch;
  
  for(p--; p >= sentence; p--){
    if(*p == ' ' || p == sentence){
      if(p == sentence)
        p--;
      for(p2 = p + 1; *p2 != ' ' && p2 <= sentence + length; p2++)
        putchar(*p2);
      if(p + 1 != sentence)
        putchar(' ');
    }
  }
  printf("%c\n", term_char);

  return 0;
}