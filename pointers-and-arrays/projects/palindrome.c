// also counts as project 4 since it's already simplifie
#include <stdio.h>
#include <stdbool.h>
#include <ctype.h>

#define MSG_LEN 100

void fill_arrays(char *message, char *reverse);
bool palindrome_check(char *message, char *reverse);

int main(void){
  char message[MSG_LEN], reverse[MSG_LEN];

  printf("Enter a message: ");
  fill_arrays(message, reverse);

  if(palindrome_check(message, reverse))
    printf("Palindrome\n");
  else
    printf("Not a palindrome\n");

  return 0;
}

void fill_arrays(char *message, char *reverse){
  char *p1, *p2, ch;

  for(p1 = message; p1 < message + MSG_LEN
      && (ch = toupper(getchar())) != '\n';)
    if(isalpha(ch))
      *p1++ = ch;
  *p1 = '\n';

  // p1-- ensures the final char of reverse is \n, even if it exceeds MSG_LEN
  for(p1--, p2 = reverse; p1 >= message; p1--, p2++)
    *p2 = *p1;
  *p2 = '\n';
}

bool palindrome_check(char *message, char *reverse){
  char *p1, *p2;
  for(p1 = message, p2 = reverse; *p1 != '\n'; p1++, p2++)
    if(*p1 != *p2)
      return false;
  return true;
}
