#include <stdio.h>

int sum_array(const int a[], int n);

int main(void){
  int n, *p;
  printf("Enter array length: ");
  scanf("%d", &n);
  int a[n];

  printf("Enter array elements: ");
  for(p = a; p < a + n;)
    scanf(" %d", p++);
  
  printf("Sum of elements: %d\n", sum_array(a, n));

  return 0;
}

int sum_array(const int a[], int n){
  int *p, sum;

  sum = 0;
  for(p = a; p < a + n;)
    sum += *(p++);
  return sum;
}
