#include <stdio.h>

void store_zeros(int a[], int n);

int main(void){
  int n, *p;
  printf("Enter array length: ");
  scanf("%d", &n);
  int a[n];

  printf("Enter array elements: ");
  for(p = a; p < a + n;)
    scanf(" %d", p++);
  
  store_zeros(a, n);
  printf("Array elements:");
  for(p = a; p < a + n;)
    printf(" %d", *(p++));

  return 0;
}

void store_zeros(int a[], int n){
  int *p;
  for(p = a; p < a + n;)
    *(p++) = 0;
}
