`
double init_ident(int n, double ident[n][n]){
  int i;
  double p = ident[0];

  for(*p++ = 1, i = 0; p < ident[0] + N * N; p++){
    if(i == N){
      *p = 1;
      i = 0;
    }
    else{
      *p = 0;
      i++;
    }
  }
}
`
