#include <stdio.h>

int *find_middle(int *a, int n){
  return a + n / 2;
}

int main(void){
  int n, *p;
  printf("Enter array length: ");
  scanf("%d", &n);
  int a[n];

  printf("Enter array elements: ");
  for(p = a; p < a + n;)
    scanf(" %d", p++);

  printf("Address of middle number: %p\nMiddle number: %d\n", find_middle(a, n), *find_middle(a, n));

  return 0;
}
