#include <stdio.h>

int find_largest(int a[], int n){
  int *p = a, max;

  max = *p;
  for(; p < a + n; p++)
    if(*p > max)
      max = *p;
  return max;
}

int main(void){
  int n, *p;
  printf("Enter array length: ");
  scanf("%d", &n);
  int a[n];

  printf("Enter array elements: ");
  for(p = a; p < a + n;)
    scanf(" %d", p++);

  printf("Largest: %d\n", find_largest(a, n));

  return 0;
}
