#include <stdio.h>
#include <stdbool.h>

bool search(const int a[], int n, int key);

int main(void){
  int n, key, *p;
  printf("Enter array length: ");
  scanf("%d", &n);
  int a[n];

  printf("Enter array elements: ");
  for(p = a; p < a + n;)
    scanf(" %d", p++);
  
  printf("Enter search key: ");
  scanf("%d", &key);

  if(search(a, n, key))
    printf("Key exists.\n");
  else
    printf("Key not found.\n");

  return 0;
}

bool search(const int a[], int n, int key){
  int *p;
  for(p = a; p < a + n;)
    if(*(p++) == key)
      return true;
  return false;
}
