#include <stdio.h>

int evaluate_position(char *board);

int main(void){
  int i, j;
  char board[8][8] = {
    {'R', 'N', 'B', 'Q', 'K', 'B', 'N', 'R'},
    [7][0] = 'r', [7][1] = 'n', [7][2] = 'b', [7][3] = 'q', [7][4] = 'k',
      [7][5] = 'b', [7][6] = 'n', [7][7] = 'r'
  };
  for(i = 0; i < 8; i++)
    board[1][i] = 'P';
  for(i = 0; i < 8; i++)
    board[6][i] = 'p';

  for(i = 2; i < 6; i++){ // only fill empty rows with '.'
    for(j = 0; j < 8; j++)
      board[i][j] = '.';
  }

  for(i = 0; i < 8; i++){
    for(j = 0; j < 8; j++)
      printf("%c ", board[i][j]);
    putchar('\n');
  }

  printf("\nMaterial advantage: %d\n", evaluate_position(board[0]));

  return 0;
}

int evaluate_position(char *board){
  int advantage = 0;
  char *p;

  for(p = board; p < board + 64; p++) // 8 rows, 8 columns; 8 * 8 = 64
    switch(*p){
      case 'Q':
        advantage += 9;
        break;
      case 'R':
        advantage += 5;
        break;
      case 'B':
        advantage += 3;
        break;
      case 'N':
        advantage += 3;
        break;
      case 'P':
        advantage += 1;
        break;

      case 'q':
        advantage -= 9;
        break;
      case 'r':
        advantage -= 5;
        break;
      case 'b':
        advantage -= 3;
        break;
      case 'n':
        advantage -= 3;
        break;
      case 'p':
        advantage -= 1;
        break;
      
      default:
        break;
    }

  return advantage;
}
