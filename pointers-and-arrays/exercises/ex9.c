#include <stdio.h>

double inner_product(const double *a, const double *b, int n);

int main(void){
  int n;
  printf("Enter array length: ");
  scanf("%d", &n);
  double a[n], b[n], *p;

  printf("Enter first array elements: ");
  for(p = a; p < a + n;)
    scanf(" %lf", p++);
  printf("Enter second array elements: ");
  for(p = b; p < b + n;)
    scanf(" %lf", p++);

  printf("Inner product: %lf\n", inner_product(a, b, n));

  return 0;
}

double inner_product(const double *a, const double *b, int n){
  double *pa, *pb, product = 0;
  for(pa = a, pb = b; pa < a + n;)
    product += *(pa++) * *(pb++);
  return product;
}
