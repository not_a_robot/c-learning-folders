This statement is illegal since pointer arithmetic is limited to addition and
subtraction of pointers and integers or pointers and pointers (no
multiplication or division).
This effect can also be produced with this line:
`
middle = low + (high - low) / 2;
`
