#include <stdio.h>

void find_two_largest(const int *a, int n, int *largest, int *second_largest){
  int *p = a;

  if(*p > *(p + 1)){
    *largest = *p;
    *second_largest = *++p;
  }
  else{
    *second_largest = *p;
    *largest = *++p;
  }

  for(p++; p < a + n; p++)
    if(*p > *largest){
      *second_largest = *largest;
      *largest = *p;
    }
}

int main(void){
  int n, *p, largest, second_largest;
  printf("Enter array length: ");
  scanf("%d", &n);
  int a[n];

  printf("Enter array elements: ");
  for(p = a; p < a + n;)
    scanf(" %d", p++);

  find_two_largest(a, n, &largest, &second_largest);
  printf("Largest: %d\nSecond largest: %d\n", largest, second_largest);

  return 0;
}
