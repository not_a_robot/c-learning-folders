/* Classifies a poker hand */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define NUM_RANKS 13
#define NUM_SUITS 4
#define NUM_CARDS 5

// external variables
int hand[NUM_CARDS][2];
bool straight, flush, four, three;
int pairs; // can be 0, 1, or 2

// prototypes
void read_cards(void);
void analyze_hand(void);
void print_result(void);
void sort_by_rank(int hand[NUM_CARDS][2], int length);

/* main: Calls read_cards, analyze_hand, and print_result repeatedly. */
int main(void){
  for(;;){
    read_cards();
    analyze_hand();
    print_result();
  }
}

/* read_cards: Reads the cards into the external variable hand; checks for bad
 * cards and duplicate cards.
 */
void read_cards(void){
  char ch, rank_ch, suit_ch;
  int rank, suit;
  bool bad_card, duplicate;
  int cards_read = 0;

  for(rank = 0; rank < NUM_RANKS; rank++){
    for(suit = 0; suit < NUM_SUITS; suit++)
      hand[rank][suit] = false;
  }

  while(cards_read < NUM_CARDS){
    bad_card = false;
    duplicate = false;

    printf("Enter a card: ");

    rank_ch = getchar();
    switch(rank_ch){
      case '0':             exit(EXIT_SUCCESS);
      case '2':             rank = 0; break;
      case '3':             rank = 1; break;
      case '4':             rank = 2; break;
      case '5':             rank = 3; break;
      case '6':             rank = 4; break;
      case '7':             rank = 5; break;
      case '8':             rank = 6; break;
      case '9':             rank = 7; break;
      case 't': case 'T':   rank = 8; break;
      case 'j': case 'J':   rank = 9; break;
      case 'q': case 'Q':   rank = 10; break;
      case 'k': case 'K':   rank = 11; break;
      case 'a': case 'A':   rank = 12; break;
      default:              bad_card = true;
    }

    suit_ch = getchar();
    switch(suit_ch){
      case 'c': case 'C': suit = 0; break;
      case 'd': case 'D': suit = 1; break;
      case 'h': case 'H': suit = 2; break;
      case 's': case 'S': suit = 3; break;
      default:            bad_card = true;
    }

    while((ch = getchar()) != '\n')
      if(ch != ' ') bad_card = true;


    for(int i = 0; i < cards_read; i++)
      if(hand[i][0] == rank && hand[i][1] == suit){
        duplicate = true;
        break;
      }
    if(bad_card)
      printf("Bad card; ignored.\n");
    else if(duplicate)
      printf("Duplicate card; ignored.\n");
    else{
      hand[cards_read][0] = rank;
      hand[cards_read][1] = suit;
      cards_read++;
    }
  }
}

/* analyze_hand: Determines whether the hand contains a straight, a flush,
 * four-of-a-kind and/or three-of-a-kind; determines number of pairs; stores
 * the results into the external variables straight, flush, four, three and
 * pairs.
 */
void analyze_hand(void){
  int rank, i, same;

  straight = true;
  flush = false;
  four = false;
  three = false;
  pairs = 0;

  sort_by_rank(hand, NUM_CARDS);

  // check for flush
  for(i = 1; i < NUM_CARDS; i++)
    if(hand[i][1] == hand[i - 1][1])
      flush = true;

  // check for straight
  for(i = 0; i < NUM_CARDS - 1; i++)
    if(hand[i][0] + 1 != hand[i + 1][0])
      straight = false;

  // check for 4-of-a-kind, 3-of-a-kind, and pairs
  for(same = rank = 0; rank < NUM_RANKS; rank++, same = 0){
    for(i = 0; i < NUM_CARDS; i++)
      if(hand[i][0] == rank) same++;
    if(same == 4) four = true;
    if(same == 3) three = true;
    if(same == 2) pairs++;
  }
}

void print_result(void){
  if(straight && flush) printf("Straight flush");
  else if(four)         printf("Four of a kind");
  else if(three &&
          pairs == 1)   printf("Full house");
  else if(flush)        printf("Flush");
  else if(straight)     printf("Straight");
  else if(three)        printf("Three of a kind");
  else if(pairs == 2)   printf("Two pairs");
  else if(pairs == 1)   printf("Pair");
  else                  printf("High card");

  printf("\n\n");
}

/* sort_by_rank: sort the cards by rank from smallest to largest using a
 * recursive selection sort algorithm (in order to be able to check for a
 * straight)
 */
void sort_by_rank(int hand[NUM_CARDS][2], int length){
  if(length == 0)
    return;

  int i, index = 0, tmp;
  for(i = 0; i < length; i++){
    if(hand[i][0] > hand[index][0])
      index = i;
  }

  // swap the largest number with the last number
  tmp = hand[length - 1][0];
  hand[length - 1][0] = hand[index][0];
  hand[index][0] = tmp;

  sort_by_rank(hand, length - 1);
}
