#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#define STACK_SIZE 100

char contents[STACK_SIZE];
int top = 0;

void stack_overflow(void);
void stack_underflow(void);

void make_empty(void);
bool is_empty(void);
bool is_full(void);
void push(char i);
char pop(void);

int main(void){
  char ch, popped;

  printf("Enter parentheses and/or braces: ");
  do{
    // scanf(" %c ", &ch);
    ch = getchar();
    if(ch == '(' || ch == '{')
      push(ch);
    else if(ch != '\n'){
      popped = pop();
      if((popped == '(' && ch == ')') || (popped == '{' && ch == '}'))
        ;
      else{
        printf("Parentheses/braces are not nested properly.\n");
        break;
      }
    }
  }while(ch != '\n');

  if(is_empty())
    printf("Parentheses/braces are nested properly.\n");
  else
    printf("Parentheses/braces are not nested properly.\n");

  return 0;
}

void make_empty(void){
  top = 0;
}

bool is_empty(void){
  return top == 0;
}

bool is_full(void){
  return top == STACK_SIZE;
}

void push(char i){
  if(is_full())
    stack_overflow();
  else
    contents[top++] = i;
}

char pop(void){
  if(is_empty())
    stack_underflow();
  else
    return contents[--top];
}

void stack_overflow(void){
  printf("\nStack overflow\n");
  exit(127);
}

void stack_underflow(void){
  printf("\nParentheses/braces aren't nested properly\n");
  exit(126);
}
