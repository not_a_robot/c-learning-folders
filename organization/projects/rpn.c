/* rpn.c: this is a calculator that can be used with Reverse Polish Notation
 * (RPN), where operators are placed after their operands instead of inbetween.
 */
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#define STACK_SIZE 100

void make_empty(void);
bool is_empty(void);
bool is_full(void);
void push(int i);
int pop(void);
void stack_overflow(void);
void stack_underflow(void);

int expression[STACK_SIZE];
int top = 0;

int main(void){
  char ch;
  int arg1, arg2;

  printf("Enter an RPN expression: ");

  while(true){
    scanf(" %c", &ch);

    if(ch >= '0' && ch <= '9')
      push(ch - '0');
    else{
      switch(ch){
        case '+':
          push(pop() + pop());
          break;
        case '-':
          arg2 = pop();
          arg1 = pop();
          push(arg1 - arg2);
          break;
        case '*':
          push(pop() * pop());
          break;
        case '/':
          arg2 = pop();
          arg1 = pop();
          push(arg1 / arg2);
          break;
        case '=':
          printf("Value of expression: %d\nEnter an RPN expression: ", pop());
          break;
        default:
          exit(0);
      }
    }
  }

  return 0;
}

void make_empty(void){
  top = 0;
}

bool is_empty(void){
  return top == 0;
}

bool is_full(void){
  return top == STACK_SIZE;
}

void push(int i){
  if(is_full())
    stack_overflow();
  else
    expression[top++] = i;
}

int pop(void){
  if(is_empty())
    stack_underflow();
  else
    return expression[--top];
}

void stack_overflow(void){
  printf("Expression is too complex\n");
  exit(74);
}
void stack_underflow(void){
  printf("Not enough operands in expression\n");
  exit(64);
}
