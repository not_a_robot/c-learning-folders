#include <stdio.h>

#define MAX_DIGITS 10

/*
 * segments: this array contains the "on" and "off" values for each segment for
 * each number. each number is a 3 * 3 matrix, going from left to right, with 1
 * indicating that that segment is turned on. Middle segments are all
 * underscores, and the first and last segment of the first row are always off,
 * since only the underscore there is needed.
 *   012
 * 0  _
 * 1 |_|
 * 2 |_|
 */
const int segments[10][9] = {
  {0, 1, 0, 1, 0, 1, 1, 1, 1}, // 0
  {0, 0, 0, 0, 0, 1, 0, 0, 1}, // 1
  {0, 1, 0, 0, 1, 1, 1, 1, 0}, // 2
  {0, 1, 0, 0, 1, 1, 0, 1, 1}, // 3
  {0, 0, 0, 1, 1, 1, 0, 0, 1}, // 4
  {0, 1, 0, 1, 1, 0, 0, 1, 1}, // 5
  {0, 1, 0, 1, 1, 0, 1, 1, 1}, // 6
  {0, 1, 0, 0, 0, 1, 0, 0, 1}, // 7
  {0, 1, 0, 1, 1, 1, 1, 1, 1}, // 8
  {0, 1, 0, 1, 1, 1, 0, 1, 1}  // 9
};

char digits[3][MAX_DIGITS * 4];

void clear_digits_array(void);
void process_digit(int digit, int position);
void print_digits_array(void);

int main(void){
  int position;
  char ch;

  printf("Enter a number: ");
  clear_digits_array();
  for(position = 0; ((ch = getchar()) != '\n') && position < MAX_DIGITS;){
    if(ch >= '0' && ch <= '9'){
      process_digit(ch - '0', position);
      position++;
    }
  }
  print_digits_array();

  return 0;
}

/*
 * clear_digits_array: fill the digits array with spaces.
 */
void clear_digits_array(void){
  int row, column;
  for(row = 0; row < 3; row++)
    for(column = 0; column < MAX_DIGITS * 4; column++)
      digits[row][column] = ' ';
}

/*
 * process_digit: match a number to its corresponding segment representation
 * from segments, and then store that into the digits array
 */
void process_digit(int digit, int position){
  int row, column, check;

  for(row = check = 0; row < 3; row++)
    for(column = position * 4; column < position * 4 + 3; column++, check++)
      if(segments[digit][check])
        switch(check){
          case 1: case 4: case 7:
            digits[row][column] = '_'; break;
          default:
            digits[row][column] = '|'; break;
        }
}

/*
 * print_digits_array: print each column in each row in the digits array.
 */
void print_digits_array(void){
  int row, column;
  for(row = 0; row < 3; row++){
    for(column = 0; column < MAX_DIGITS * 4; column++)
      printf("%c", digits[row][column]);
    putchar('\n');
  }
}
