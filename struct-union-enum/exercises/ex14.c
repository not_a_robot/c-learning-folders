#include <stdio.h>

#define RECTANGLE 1
#define CIRCLE 2
#define PI 3.14159

struct point{
  int x, y;
};

struct shape{
	int shape_kind;				/* RECTANGLE or CIRCLE */
	struct point center;	/* coordinates of center */
	union{
		struct{
			int height, width;
		} rectangle;
		struct{
			int radius;
		} circle;
	} u;
};

int area(struct shape);
struct shape move(struct shape, int x, int y);
struct shape scale(struct shape, double factor);

int
main(void){
	struct shape s;

	printf("Shape kind (rectangle is %d, circle is %d)? ", RECTANGLE, CIRCLE);
	scanf("%d", &s.shape_kind);
	printf("Center coordinates? ");
	scanf("%d, %d", &s.center.x, &s.center.y);
	if(s.shape_kind == RECTANGLE){
		printf("Enter height: ");
		scanf("%d", &s.u.rectangle.height);
		printf("Enter width: ");
		scanf("%d", &s.u.rectangle.width);
	}
	else{
		printf("Enter radius: ");
		scanf("%d", &s.u.circle.radius);
	}
}

int
area(struct shape s){
	if(s.shape_kind == RECTANGLE)
		return s.u.rectangle.height * s.u.rectangle.width;
	return (int) PI * s.u.circle.radius * s.u.circle.radius;
}

struct shape
move(struct shape s, int x, int y){
  s.center.x += x;
  s.center.y += y;

  return s;
}

struct shape
scale(struct shape s, double factor){
  if(s.shape_kind == RECTANGLE){
    s.u.rectangle.height *= factor;
    s.u.rectangle.width *= factor;
  }
	else
    s.u.circle.radius *= factor;

  return s;
}
