#include <stdio.h>
#include <stdbool.h>

struct color{
  int red;
  int green;
  int blue;
};

struct color make_color(int red, int green, int blue);
int getRed(struct color c);
bool equal_color(struct color color1, struct color color2);
struct color brighter(struct color c);
struct color darker(struct color c);

int
main(void){
  int red, green, blue;
  struct color my_color;

  printf("Enter red: ");
  scanf("%d", &red);
  printf("Enter green: ");
  scanf("%d", &green);
  printf("Enter blue: ");
  scanf("%d", &blue);

  my_color = make_color(red, green, blue);

  return 0;
}

struct color
make_color(int red, int green, int blue){
  struct color c = {
    .red = red < 0 ? 0 : red > 255 ? 255 : red,
    .green = green < 0 ? 0 : green > 255 ? 255 : green,
    .blue = blue < 0 ? 0 : blue > 255 ? 255 : blue
  };

  return c;
}

int
getRed(struct color c){
  return c.red;
}

bool
equal_color(struct color color1, struct color color2){
  if(color1.red == color2.red && color1.green == color2.green &&
      color1.blue == color2.blue)
    return true;
  return false;
}

struct color brighter(struct color c){
  if(c.red == 0 && c.green == 0 && c.blue == 0)
    c.red = c.green = c.blue = 3;
  else{
    c.red = c.red > 0 && c.red < 3 ? 3 / 0.7 : c.red / 0.7;
    c.green = c.green > 0 && c.green < 3 ? 3 / 0.7 : c.green / 0.7;
    c.blue = c.blue > 0 && c.blue < 3 ? 3 / 0.7 : c.blue / 0.7;
  }

  c.red = c.red > 255 ? 255 : c.red;
  c.green = c.green > 255 ? 255 : c.green;
  c.blue = c.blue > 255 ? 255 : c.blue;

  return c;
}

struct color darker(struct color c){
  c.red *= 0.7;
  c.green *= 0.7;
  c.blue *= 0.7;

  return c;
}
