a)
```
struct complex{
  double real, imaginary;
};
```
b)
```
struct complex c1, c2, c3;
```
c)
```
struct complex make_complex(double real, double imaginary){
  struct complex abc = {.real = real, .imaginary = imaginary};
  return abc;
}
```
d)
```
struct complex add_complex(struct complex first, struct complex second){
  struct complex value = {first.real + second.real,
    first.imaginary + second.imaginary};
  return value;
}
```
