a)
```
typedef struct{
  double real, imaginary;
} Complex;
```
b)
```
Complex c1, c2, c3;
```
c)
```
Complex make_complex(double real, double imaginary){
  Complex abc = {.real = real, .imaginary = imaginary};
  return abc;
}
```
d)
```
Complex add_complex(Complex first, Complex second){
  Complex value = {first.real + second.real, first.imaginary + second.imaginary};
  return value;
}
```
