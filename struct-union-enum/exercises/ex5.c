#include <stdio.h>

struct date{
  int day, month, year;
};

int day_of_year(struct date d);
int compare_dates(struct date d1, struct date d2);

int
main(void){
  struct date d1, d2;

  printf("Enter 1st date (dd/mm/yyyy): ");
  scanf("%d / %d / %d", &d1.day, &d1.month, &d1.year);
  printf("Enter 2nd date (dd/mm/yyyy): ");
  scanf("%d / %d / %d", &d2.day, &d2.month, &d2.year);

  printf("Day of the year for 1st date: %d\n", day_of_year(d1));
  printf("compare_dates: %d\n", compare_dates(d1, d2));

  return 0;
}

int
day_of_year(struct date d){
  int day = d.day, i = 1;

  for(i = 1; i < d.month; i++){
    switch(i){
      case 1: case 3: case 5: case 7: case 8: case 10: case 12:
        day += 31; break;
      case 2:
        day += d.year % 4 ? 29 : 28; break;
      default:
        day += 30; break;
    }
  }

  return day;
}

int
compare_dates(struct date d1, struct date d2){
  return day_of_year(d1) + d1.year > day_of_year(d2) + d2.year ? 1 :
    day_of_year(d1) + d1.year < day_of_year(d2) + d2.year ? -1 : 0;
}
