#include <stdio.h>
#include <stdbool.h>

struct point{
  int x, y;
};

struct rectangle{
  struct point upper_left, lower_right;
};

int area(struct rectangle);
struct point center(struct rectangle);
struct rectangle move(struct rectangle, int x, int y);
bool in_rectangle(struct rectangle, struct point);

int
main(void){
  struct rectangle r;

  printf("Upper left coordinates? ");
  scanf("%d, %d", &r.upper_left.x, &r.upper_left.y);
  printf("Lower right coordinates? ");
  scanf("%d, %d", &r.lower_right.x, &r.lower_right.y);

  printf("Area of the rectangle: %d\n", area(r));

  return 0;
}

int
area(struct rectangle r){
  int length = r.upper_left.y - r.lower_right.y,
      width = r.lower_right.x - r.upper_left.x;

  return length * width;
}

struct point
center(struct rectangle r){
  struct point middle = {
    .x = (r.upper_left.x + r.lower_right.x) / 2,
    .y = (r.upper_left.y + r.lower_right.y) / 2
  };

  return middle;
}

struct rectangle
move(struct rectangle r, int x, int y){
  r.upper_left.x += x;
  r.upper_left.y += y;
  r.lower_right.x += x;
  r.lower_right.y += y;

  return r;
}

bool
in_rectangle(struct rectangle r, struct point p){
  return p.x > r.upper_left.x && p.x < r.lower_right.x &&
         p.y < r.upper_left.y && p.y > r.lower_right.y;
}
