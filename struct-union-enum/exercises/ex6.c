#include <stdio.h>

struct time{
  int hours, minutes, seconds;
};

struct time split_time(long total_seconds);

int
main(void){
  long seconds;
  struct time splitted;

  printf("Enter number of seconds since midnight: ");
  scanf("%ld", &seconds);

  splitted = split_time(seconds);
  printf("Hours: %d\nMinutes: %d\nSeconds: %d\n",
    splitted.hours, splitted.minutes, splitted.seconds);

  return 0;
}

struct time
split_time(long total_seconds){
  struct time splitted = {
    .hours = total_seconds / 60 / 60,
    .minutes = (total_seconds - splitted.hours * 3600) / 60,
    .seconds = (total_seconds - splitted.hours * 3600 - splitted.minutes * 60)
  };

  return splitted;
}
