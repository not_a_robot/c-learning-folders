#include <stdio.h>

struct fraction{
  int numerator, denominator;
};

struct fraction reduce(struct fraction f);
struct fraction add_frac(struct fraction f1, struct fraction f2);
struct fraction sub_frac(struct fraction f1, struct fraction f2);
struct fraction multiply_frac(struct fraction f1, struct fraction f2);
struct fraction divide_frac(struct fraction f1, struct fraction f2);

int
main(void){
  struct fraction f1, f2, f3;

  printf("Enter fraction 1: ");
  scanf("%d / %d", &f1.numerator, &f1.denominator);
  printf("Enter fraction 2: ");
  scanf("%d / %d", &f2.numerator, &f2.denominator);

  f3 = reduce(f1);
  printf("Reduced: %d/%d\n", f3.numerator, f3.denominator);

  f3 = add_frac(f1, f2);
  printf("%d/%d + %d/%d = %d/%d\n", f1.numerator, f1.denominator,
    f2.numerator, f2.denominator, f3.numerator, f3.denominator);

  f3 = sub_frac(f1, f2);
  printf("%d/%d - %d/%d = %d/%d\n", f1.numerator, f1.denominator,
    f2.numerator, f2.denominator, f3.numerator, f3.denominator);

  f3 = multiply_frac(f1, f2);
  printf("%d/%d * %d/%d = %d/%d\n", f1.numerator, f1.denominator,
    f2.numerator, f2.denominator, f3.numerator, f3.denominator);

  f3 = divide_frac(f1, f2);
  printf("%d/%d / %d/%d = %d/%d\n", f1.numerator, f1.denominator,
    f2.numerator, f2.denominator, f3.numerator, f3.denominator);

  return 0;
}

struct fraction
reduce(struct fraction f){
  int n = f.numerator, gcd = f.denominator, remainder;

  while(n != 0){
    remainder = gcd % n;
    gcd = n;
    n = remainder;
  }
  f.numerator /= gcd; f.denominator /= gcd;

  return f;
}

struct fraction
add_frac(struct fraction f1, struct fraction f2){
  int denom = f1.denominator;

  f1.numerator *= f2.denominator;
  f1.denominator *= f2.denominator;

  f2.numerator *= denom;
  f2.denominator *= denom;

  f1.numerator += f2.numerator;

  return reduce(f1);
}

struct fraction
sub_frac(struct fraction f1, struct fraction f2){
  int denom = f1.denominator;

  f1.numerator *= f2.denominator;
  f1.denominator *= f2.denominator;

  f2.numerator *= denom;
  f2.denominator *= denom;

  f1.numerator -= f2.numerator;

  return reduce(f1);
}

struct fraction
multiply_frac(struct fraction f1, struct fraction f2){
  f1.numerator *= f2.numerator;
  f1.denominator *= f2.denominator;

  return reduce(f1);
}

struct fraction
divide_frac(struct fraction f1, struct fraction f2){
  f1.numerator *= f2.denominator;
  f1.denominator *= f2.numerator;

  return reduce(f1);
}
