/* This program asks the user to enter a time and display the departure
 * time of a plane from a table from the exercise. The time is converted
 * to minutes only, so it becomes easier to use math on it.
 */
#include <stdio.h>
#include <stdbool.h>

#define NUM_TIMES 8


int
main(void){
  int hours, mins, total, i,
      hours_dep, hours_ariv, mins_dep, mins_ariv;
  bool after_mid_dep, after_mid_ariv;
  struct{
    int departure;
    int arrival;
  } times[NUM_TIMES] = {
    {480, 616},   // 8:00 am, 10:16 am
    {583, 712},   // 9:43 am, 11:52 am
    {679, 811},   // 11:19 am, 1:31 pm
    {767, 900},   // 12:47 pm, 3:00 pm
    {840, 968},   // 2:00 pm, 4:08 pm
    {945, 1075},  // 3:45 pm, 5:55 pm
    {1140, 1280}, // 7:00 pm, 9:20 pm
    {1305, 1438}  // 9:45 pm, 11:58 pm
  };

  printf("Enter a 24-hour time: ");
  scanf("%d:%d", &hours, &mins);
  total = hours * 60 + mins;

  for(i = 0; i < NUM_TIMES; i++){
    if(total <= (times[i].departure + times[i+1].departure) / 2){
      hours_dep = times[i].departure / 60;
      mins_dep = times[i].departure - hours_dep * 60;

      hours_ariv = times[i].arrival / 60;
      mins_ariv = times[i].arrival - hours_ariv * 60;

      hours_dep = (after_mid_dep = hours_dep > 12) ?
        hours_dep - 12 : hours_dep;
      hours_ariv = (after_mid_ariv = hours_ariv > 12) ?
        hours_ariv - 12 : hours_ariv;

      printf("Closest departure time is %d:%.2d %s, arriving at %d:%.2d %s.\n",
          hours_dep, mins_dep, after_mid_dep ? "pm" : "am",
          hours_ariv, mins_ariv, after_mid_ariv ? "pm" : "am");
      break;
    }
  }

  return 0;
}
