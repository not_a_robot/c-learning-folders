/* This program prompts the user to enter two dates and indicates which
 * comes earlier on the calendar.
 */
#include <stdio.h>

struct date{
  int day;
  int month;
  int year;
};


int day_of_year(struct date d);
int compare_dates(struct date d1, struct date d2);

int
main(void){
  int comparision;
  struct date date1, date2, tmp;

  printf("Enter first date (dd/mm/yy): ");
  scanf("%d/%d/%d", &date1.day, &date1.month, &date1.year);
  printf("Enter second date (dd/mm/yy): ");
  scanf("%d/%d/%d", &date2.day, &date2.month, &date2.year);

  comparision = compare_dates(date1, date2);

  if(comparision == -1){
    tmp = date2;
    date2 = date1;
    date1 = tmp;
  }

  printf("%.2d/%.2d/%.4d is earlier than %.2d/%.2d/%.4d\n",
      date1.day, date1.month, date1.year,
      date2.day, date2.month, date2.year);

  return 0;
}

int
day_of_year(struct date d){
  int day = d.day, i = 1;

  for(i = 1; i < d.month; i++){
    switch(i){
      case 1: case 3: case 5: case 7: case 8: case 10: case 12:
        day += 31; break;
      case 2:
        day += d.year % 4 ? 29 : 28; break;
      default:
        day += 30; break;
    }
  }

  return day;
}

int
compare_dates(struct date d1, struct date d2){
  return day_of_year(d1) + d1.year > day_of_year(d2) + d2.year ? 1 :
    day_of_year(d1) + d1.year < day_of_year(d2) + d2.year ? -1 : 0;
}
