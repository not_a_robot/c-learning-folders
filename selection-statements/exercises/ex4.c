#include <stdio.h>

int main(void){
  int i = 2, j, expr;

  printf("i = %d. Enter j: ", i);
  scanf("%d", &j);

  expr = (i > j) - (i < j);
  printf("%d\n", expr);

  return 0;
}
