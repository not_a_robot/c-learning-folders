.TL
Notes on Chapter 5: Selection Statements

.SH
Types of Statements
.IP \[bu]
.I "Selection statements: "
The `if` and `switch` statements allow a program to select a particular execution path from a set of alternatives.
.IP \[bu]
.I "Iteration statements: "
The `while`, `do`, and `for` statements support iteration (i.e looping).
.IP \[bu]
.I "Jump statements: "
The `break`, `continue`, `return`, and `goto` statements cause an unconditional jump to some other place in the program.

.SH
Relational Operators
.IP \[bu]
Unlike many programming languages, expressions like 
.I "i < j "
don't return a "Boolean" or "logical" type, but return either 0 for false, and 1 for true.
.IP \[bu]
Relational operators can compare both integers and floating-point numbers, and those can be mixed together.
.IP \[bu]
The precedence of relational operators is lower than that of arithmetic operators, and are left associative.

.SH
Equality Operators
.IP \[bu]
Equality operators are left associative and return either 0 or 1.
.IP \[bu]
They are of 
.I "lower"
precednce than relational operators. e.g:

.fam P
.I "i < j == j < k"
.fam
.IP
is equivalent to:

.fam P
.I "(i < j) == (j < k)"
.fam
.IP
which is true (returns 1) if `i < j` and `j < k` are both true or both false.

.SH
Logical Operators
.IP \[bu]
More complicated logical expressions can be built from simpler ones using the 
.BI "logical operators: "
.I "and, or, "
and
.I "not, "
which are 
.I "!, &&, "
and
.I "||"
, respectively.
.IP \[bu]
!
.I "expr "
has the value of 1 if 
.I "expr "
has the value of 0.
.IP \[bu]
.I "expr1 "
&& 
.I "expr2 "
has the value of 1 if the values of both 
.I "expr1 "
and 
.I "expr2 "
are non-zero.
.IP \[bu]
.I "expr1 "
|| 
.I "expr2 "
has the value of 1 if either 
.I "expr1 "
or 
.I "expr2 "
(or both) has a non-zero value.
.IP \[bu]
In all other cases, these operators produce the value 0.

.SH
Conditional Expressions
.IP \[bu]
The 
.BI "conditional operator "
consist of two symbols, which must be used together like so:

.fam P
.I "expr1 ? expr2 : expr3"
.fam

.IP \[bu]
The expression is evaluated in stages: 
.I "expr1 "
is evaluated first; if its value isn't zero, then 
.I "expr2 "
is evaluated, and its value is the value of the entire conditional expression. If the value of 
.I "expr1 "
is zero, then the value of 
.I "expr3 "
is the value of the conditional.
