/* This program converts a numerical grade into a letter one based on
 * the values given in the exercise.
 */
#include <stdio.h>

int main(void){
  int input, tens;

  printf("Enter numerical grade: ");
  scanf("%d", &input);

  tens = input / 10;

  switch(tens){
    case 10:  case 9:
      if(input > 100)
        printf("ERROR: grade is more than 100.\n");
      else
        printf("Letter grade: A\n");
      break;
    case 8:
      printf("Letter grade: B\n");  break;
    case 7:
      printf("Letter grade: C\n");  break;
    case 6:
      printf("Letter grade: D\n");  break;
    default:
      if(input < 0)
        printf("ERROR: grade is less than 0.\n");
      else if(input > 100)
        printf("ERROR: grade is more than 100.\n");
      else
        printf("Letter grade: F\n");
      break;
  }

  return 0;
}
