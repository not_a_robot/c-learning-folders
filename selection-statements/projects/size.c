#include <stdio.h>

int main(void){
  int i1, i2, i3, i4, largest, smallest, tmp;

  printf("Enter four integers: ");
  scanf("%d%d%d%d", &i1, &i2, &i3, &i4);

  if(i1 > i2){
    tmp = i1;
    i1 = i2;
    i2 = tmp;
  }

  if(i3 > i4){
    tmp = i3;
    i3 = i4;
    i4 = tmp;
  }

  if(i1 < i3){
    smallest = i1;
  }
  else{
    smallest = i3;
  }

  if(i2 > i4){
    largest = i2;
  }
  else{
    largest = i4;
  }

  printf("Largest: %d\nSmallest: %d\n", largest, smallest);

  return 0;
}
