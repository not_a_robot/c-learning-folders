/* This program asks the user to enter a time and display the departure
 * time of a plane from a table from the exercise. The time is converted
 * to minutes only, so it becomes easier to use math on it.
 */
#include <stdio.h>

int main(void){
  int hours, mins, total;

  printf("Enter a 24-hour time: ");
  scanf("%d:%d", &hours, &mins);
  total = hours * 60 + mins;

  if(total < 480 || total <= 531.5) // the second evaluation is the halfway point exactly between 8:00 and 9:43
    printf("Closest departure time is 8:00 a.m., arriving at 10:16 a.m.\n");
  else if(total <= 631) // halfway point between 9:43 and 11:19
    printf("Closest departure time 9:43 a.m., arriving at 11:52 a.m.\n");
  else if(total <= 723) // halfway point between 11:19 and 12:47
    printf("Closest departure time 11:19 a.m., arriving at 1:31 p.m.\n");
  else if(total <= 803.5) // halfway point between 12:47 and 2:00
    printf("Closest departure time 12:47 p.m., arriving at 3:00 p.m.\n");
  else if(total <= 892.5) // halfway point between 2:00 and 3:00
    printf("Closest departure time 2:00 p.m., arriving at 4:08 p.m.\n");
  else if(total <= 1042.5)  // halfway point between 3:45 and 7:00
    printf("Closest departure time 3:45 p.m., arriving at 5:55 p.m.\n");
  else if(total <= 1222.5)  // halfway point between 7:00 and 9:45
    printf("Closest departure time 7:00 p.m., arriving at 5:55 p.m.\n");
  else
    printf("Closest departure time 9:45 p.m., arriving at 11:58 p.m.\n");

  return 0;
}
