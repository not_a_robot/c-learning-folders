#include <stdio.h>

int main(void){
  int hours, mins;

  printf("Enter a 24-hour time: ");
  scanf("%d:%d", &hours, &mins);

  if(hours > 23 || mins > 59){
    printf("Invalid time.\n");
  }
  else if(hours == 12){
    hours = 12;
    printf("Equivalent 12-hour time: %d:%d PM.\n", hours, mins);
  }
  else if(hours > 12 && hours < 24){
    hours -= 12;
    printf("Equivalent 12-hour time: %d:%d PM.\n", hours, mins);
  }
  else if(hours < 12 && hours > 0){
    printf("Equivalent 12-hour time: %d:%d AM.\n", hours, mins);
  }

  return 0;
}
