#include <stdio.h>

int day_of_year(int day, int month, int year){
  // iterate through each month and add the number of days each month has to
  // the total number of days.
  for(int i = 0; i < month; i++){
    switch(i){
      case 4: case 6: case 9: case 11:
        day += 30 * i;
        break;
      case 2:
        if(year % 4 == 0) // leap years
          day += 29 * i;
        else
          day += 28 * i;
        break;
      default:
        day += 31 * i;
        break;
    }
  }

  return day;
}

int main(void){
  int day, month, year;

  printf("Enter date (d/m/y): ");
  scanf("%d / %d / %d", &day, &month, &year);

  printf("Day of the year: %d\n", day_of_year(day, month, year));

  return 0;
}
