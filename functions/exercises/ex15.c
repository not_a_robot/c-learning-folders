#include <stdio.h>

double median(double x, double y, double z);

int main(void){
  double x, y, z;

  printf("Enter 3 numbers: ");
  scanf("%lf %lf %lf", &x, &y, &z);

  printf("Median: %lf\n", median(x, y, z));

  return 0;
}

double median(double x, double y, double z){
  int median;

  if(x <= y)
    if(y <= z)
      median = y;
    else if(x <= z)
      median = z;
    else
      median = x;
  else{
    if(z <= y) median = y;
    else if(x <= z) median = x;
    else median = z;
  }

  return median;
}
