`
int num_digits(int n){
  int i;

  for(i = 0; n != 0; i++)
    n /= 10;

  return i;
}
`
