#include <stdio.h>

float compute_GPA(char grades[], int n);

int main(void){
  int i, n;

  printf("Enter array length: ");
  scanf("%d", &n);
  char grades[n], ch;

  printf("Enter array elements: ");
  getchar(); // catch stray newline
  for(i = 0; i < n && (ch = getchar()) != '\n'; i++){
    if(ch != ' ')
      grades[i] = ch;
    else
      i--;
  }

  printf("Average grade: %g\n", compute_GPA(grades, n));

  return 0;
}

float compute_GPA(char grades[], int n){
  int total, i;
  for(i = total = 0; i < n; i++){
    if(grades[i] == 'F')
      ;
    else
      total += (69 - grades[i]);
  }

  return (float) total / n;
}
