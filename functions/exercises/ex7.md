All of these statements are legal since type conversion occurs.
The last one is also legal since a function can be called without setting its
return value to a variable.
