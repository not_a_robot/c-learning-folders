`
double triangle_area(double base, height)
double product;
{
	product = base * height;
	return product / 2;
}
`
Firstly, the parameter `height` does not have a type specified, each parameter
must have its type declared before it.
Secondly, `product` is declared outside the scope of the function
`triangle_area`, so can't be used inside it. Here is the function fixed:
`
double triangle_area(double base, double height){
  double product = base * height;
  return product / 2;
}
`
