#include <stdio.h>

double innner_product(double a[], double b[], int n);

int main(void){
  int n, i;

  printf("Enter arrays' length: ");
  scanf("%d", &n);

  double a[n], b[n];

  printf("Enter a's elements: ");
  for(i = 0; i < n; i++)
    scanf("%lf", &a[i]);
  
  printf("Enter b's elements: ");
  for(i = 0; i < n; i++)
    scanf("%lf", &b[i]);
  
  printf("The \"inner product\": %g\n", innner_product(a, b, n));

  return 0;
}

double innner_product(double a[], double b[], int n){
  int i;
  double product;
  for(product = i = 0; i < n; i++)
    product += a[i] * b[i];
  
  return product;
}
