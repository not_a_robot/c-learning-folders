#include <stdio.h>

int largest(int a[], int n);
float average(int a[], int n);
int positive(int a[], int n);

int main(void){
  int n, i;

  printf("Enter length of array: ");
  scanf("%d", &n);
  int a[n];

  printf("Enter array elements: ");
  for(i = 0; i < n; i++)
    scanf("%d", &a[i]);

  printf("Contents of a:");
  for(i = 0; i < n; i++){
    printf(" %d", a[i]);
  }
  printf("\n\n");

  printf("largest element: %d\naverage: %g\nnumber of positive elements: %d\n",
    largest(a, n), average(a, n), positive(a, n));

  return 0;
}

int largest(int a[], int n){
  int i, largest;

  for(i = 0, largest = a[i]; i < n; i++){
    if(a[i] > largest)
      largest = a[i];
  }

  return largest;
}

float average(int a[], int n){
  int i, total;

  for(i = total = 0; i < n; i++){
    total += a[i];
  }

  return (float) total / n;
}

int positive(int a[], int n){
  int i, positives;

  for(i = positives = 0; i < n; i++){
    if(a[i] > 0)
      positives++;
  }

  return positives;
}
