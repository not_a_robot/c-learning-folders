#include <stdio.h>

int fact(int n);

int main(void){
  int n;

  printf("Enter a number: ");
  scanf("%d", &n);

  printf("Factorial: %d\n", fact(n));

  return 0;
}

int fact(int n){
  int i, product;

  for(i = 2, product = 1; i <= n; i++)
    product *= i;

  return product;
}
