#include <stdio.h>

void selection_sort(int integers[], int length);

int main(void){
  int length, i;

  printf("Enter length of array: ");
  scanf("%d", &length);

  int integers[length];

  printf("Enter %d array elements: ", length);
  for(i = 0; i < length; i++)
    scanf("%d", &integers[i]);

  selection_sort(integers, length);
  printf("Array elements sorted:");
  for(i = 0; i < length; i++)
    printf(" %d", integers[i]);
  putchar('\n');

  return 0;
}

void selection_sort(int integers[], int length){
  if(length == 0)
    return;

  int i, index = 0, tmp;
  for(i = 0; i < length; i++){
    if(integers[i] > integers[index])
      index = i;
  }

  // swap the largest number with the last number
  tmp = integers[length - 1];
  integers[length - 1] = integers[index];
  integers[index] = tmp;

  selection_sort(integers, length - 1);
}
