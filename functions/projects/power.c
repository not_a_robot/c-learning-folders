#include <stdio.h>

int power(int x, int n);

int main(void){
  int x, n;

  printf("Enter x: ");
  scanf("%d", &x);

  printf("Enter its power: ");
  scanf("%d", &n);

  printf("%d^%d = %d\n", x, n, power(x, n));
}

int power(int x, int n){
  int i;
  if(n == 0)
    return 1;
  else{
    if(n % 2 == 0){
      i = power(x, n / 2);
      return i * i;
    }
    else
      return x * power(x, n - 1);
  }
}
