/* This program prints a magic square whose dimensions are specified by the
 * user. A magic square is a square grid of numbers where the sums of each row,
 * column and diagonal all add up to the same number.
 */
#include <stdio.h>

void create_magic_square(int n, int magic_square[n][n]);
void print_magic_square(int n, int magic_square[n][n]);

int main(void){
	int lengths;
	printf("This program creates a magic square of a specified size.\n");
  printf("The size must be an odd number between 1 and 99.\n");

  printf("Enter size of magic square: ");
  scanf("%d", &lengths);

  int grid[lengths][lengths];

  create_magic_square(lengths, grid);
  print_magic_square(lengths, grid);

  return 0;
}

void create_magic_square(int n, int magic_square[n][n]){
  int row, column, prev_row, prev_column, i;

  // fill grid with 0s
  for(row = 0; row < n; row++){
    for(column = 0; column < n; column++)
      magic_square[row][column] = 0;
  }

  for(i = 1, row = 0, column = n / 2; i <= (n * n); i++, column++, row--){
    // else if statements invalid, two events can happen simultaenously
    if(column >= n) // don't go out of bounds
      column = 0;
    if(row < 0) // don't go out of bounds
      row = n - 1;
    if(magic_square[row][column] != 0){ // put number below previous number
      row = prev_row + 1;
      column = prev_column;
    }

    magic_square[row][column] = i;

    prev_row = row;
    prev_column = column;
  }
}

void print_magic_square(int n, int magic_square[n][n]){
  int row, column;

  for(row = 0; row < n; row++){
    for(column = 0; column < n; column++)
      printf("%5d", magic_square[row][column]);
    printf("\n");
  }
}