#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define GRID_ROWS 10
#define GRID_COLUMNS 10

void generate_random_walk(char grid[GRID_ROWS][GRID_COLUMNS]);
void print_array(char grid[GRID_ROWS][GRID_COLUMNS]);

int main(void){
  char grid[GRID_ROWS][GRID_COLUMNS];

  generate_random_walk(grid);

  print_array(grid);

  return 0;
}

void generate_random_walk(char grid[GRID_ROWS][GRID_COLUMNS]){
  int row, column, direction, success;
  char ch;

  // fill grid with '.'
  for(row = 0; row < GRID_ROWS; row++){
    for(column = 0; column < GRID_COLUMNS; column++){
      grid[row][column] = '.';
    }
  }

  srand((unsigned) time(NULL));

  for(ch = 'A' - 1, row = column = 0, success = 1; ch < 'Z';){
    if(success == 1){
      ch++;
      grid[row][column] = ch;
    }
    direction = rand() % 4;

    // break out of the loop if four directions are blocked
    if((grid[row + 1][column] != '.' && grid[row - 1][column] != '.') &&
        (grid[row][column + 1] != '.' && grid[row][column - 1] != '.'))
      break;

    while(direction == 0){ // 0 moves up
      if(row == 0 || grid[row - 1][column] != '.'){
        success = 0;
        direction = rand() % 4;
        continue;
      }
      else{
        success = 1;
        row--;
        break;
      }
    }

    while(direction == 1){ // 1 moves right
      if(column == 9 || grid[row][column + 1] != '.'){
        success = 0;
        direction = rand() % 4;
        continue;
      }
      else{
        success = 1;
        column++;
        break;
      }
    }

    while(direction == 2){ // 2 moves down
      if(row == 9 || grid[row + 1][column] != '.'){
        success = 0;
        direction = rand() % 4;
        continue;
      }
      else{
        success = 1;
        row++;
        break;
      }
    }

    while(direction == 3){ // 3 moves left
      if(column == 0 || grid[row][column - 1] != '.'){
        success = 0;
        direction = rand() % 4;
        continue;
      }
      else{
        success = 1;
        column--;
        break;
      }
    }
  }
}

void print_array(char grid[GRID_ROWS][GRID_COLUMNS]){
  int row, column;

  for(row = 0; row < GRID_ROWS; row++){
    for(column = 0; column < GRID_COLUMNS; column++){
      printf("%c ", grid[row][column]);
    }
    printf("\n");
  }

}
