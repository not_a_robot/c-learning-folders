#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

int roll_dice(void);
bool play_game(void);

int main(void){
  int wins = 0, losses = 0;
  char ch;
  bool again = true;

  while(again){

    if(play_game()){
      printf("You win!\n\n");
      wins++;
    }
    else{
      printf("You lose!\n\n");
      losses++;
    }

    printf("Play again? ");
    if((ch = getchar()) == 'y' || ch == 'Y'){
      again = true;
      getchar(); // catch stray newline
    }
    else
      again = false;
  }

  printf("\nWins: %d\tLosses: %d\n", wins, losses);

  return 0;
}

int roll_dice(void){
  int dice1 = 0, dice2 = 0;

  srand((unsigned) time(NULL));
  // make sure neither dice are 0
  while(dice1 == 0)
    dice1 = rand() % 6;
  while(dice2 == 0)
    dice2 = rand() % 6;

  return dice1 + dice2;
}

bool play_game(void){
  int point = roll_dice(), roll;

  printf("You rolled: %d\n", point);
  switch(point){
    case 7: case 11:
      return true;
    case 2: case 3: case 12:
      return false;
    default:
      printf("Your point is: %d\n", point);
      break;
  }

  while(true){
    roll = roll_dice();
    printf("You rolled: %d\n", roll);
    if(roll == 7)
      return false;
    else if(roll == point)
      return true;
  }
}
