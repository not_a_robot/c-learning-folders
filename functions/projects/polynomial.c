#include <stdio.h>

int polynomial(int x);

int main(void){
  int x;

  printf("Enter the value of x: ");
  scanf("%d", &x);

  printf("3x^5 + 2x^4 - 5x^3 - x^2 + 7x - 6 = %d\n", polynomial(x));

  return 0;
}

int polynomial(int x){
  int i, tmp;
  int total;

  for(i = 2, tmp = x; i <= 5; i++){ // find value of x^5
    tmp *= x;
    printf("\nx^%d = %d\n", i, tmp);
  }
  total = 3 * tmp; // 3x^5

  for(i = 2, tmp = x; i <= 4; i++){ // find value of x^4
    tmp *= x;
  }
  total += 2 * tmp; // + 2x^4

  for(i = 2, tmp = x; i <= 3; i++){ // find value of x^3
    tmp *= x;
  }
  total -= 5 * tmp; // - 5x^3

  total -= x * x; // - x^2

  total += 7 * x; // + 7x

  total -= 6; // - 6

  return total;
}
