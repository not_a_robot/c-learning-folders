#include <stdio.h>

int main(void){
  int fib_numbers[39] = {1, 1}, i;

  for(i = 2; i <= (int) (sizeof(fib_numbers) / sizeof(fib_numbers[0])); i++){
    fib_numbers[i] = fib_numbers[i - 1] + fib_numbers[i - 2];
    printf("%d, ", fib_numbers[i]);
  }
  putchar(10);

  return 0;
}
