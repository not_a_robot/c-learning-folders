This is an inferior technique since the programmer will have to check and/or change the type `t` every time they add or change an array.
