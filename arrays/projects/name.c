/* This program takes a first and last name, then displays the first name, a
 * comma, and the first initial, followed by a period.
 */
#include <stdio.h>

int main(void){
  int i, length;
  char initial, ch[20];

  printf("Enter a first and last name: ");
  while((initial = getchar()) == ' ')  // only set initial when it isn't space
    ;

  while(getchar() != ' ') // ignore the rest of the first name
    ;
  // can't have something like:
  // while((ch = getchar()) != 10 & (ch = getchar()) != ' ')
  // since ch will get set twice
  for(i = 0, ch[i] = getchar(); (ch[i] != 10) & (ch[i] != ' ');
      ch[i] = getchar(), i++)
    ;
  length = i;

  printf("You entered the name: ");
  for(i = 0; i < length; i++)
    printf("%c", ch[i]);
  printf(", %c.\n", initial);

  return 0;
}
