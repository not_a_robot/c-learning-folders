#include <stdbool.h>
#include <stdio.h>

int main(void){
  bool digit_seen[10] = {false};
  int digit;
  long input, n;

  do{
    printf("Enter a number: ");
    scanf("%ld", &input);
    n = input;
  
    while(n > 0){
      digit = n % 10;
      if(digit_seen[digit])
        break;
      digit_seen[digit] = true;
      n /= 10;
    }

    if(n > 0)
      printf("Repeated digit\n");
    else
      printf("No repeated digit\n");

    // reset array
    for(digit = 0; digit < 10; digit++)
      digit_seen[digit] = false;
  }while(input != 0);

  return 0;
}
