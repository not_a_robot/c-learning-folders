/* This program prints a magic square whose dimensions are specified by the
 * user. A magic square is a square grid of numbers where the sums of each row,
 * column and diagonal all add up to the same number.
 */
#include <stdio.h>

int main(void){
	int lengths, row, column, i, prev_row, prev_column;
	printf("This program creates a magic square of a specified size.\n");
  printf("The size must be an odd number between 1 and 99.\n");

  printf("Enter size of magic square: ");
  scanf("%d", &lengths);

  int grid[lengths][lengths];
  // fill grid with 0s
  for(row = 0; row < lengths; row++){
    for(column = 0; column < lengths; column++)
      grid[row][column] = 0;
  }

  for(i = 1, row = 0, column = lengths / 2; i <= (lengths * lengths);
    i++, column++, row--){
    
    // didn't use if else statements cause two of these events can happen at
    // the same time
    if(column >= lengths) // don't go out of bounds
      column = 0;
    if(row < 0) // don't go out of bounds
      row = lengths - 1;
    if(grid[row][column] != 0){ // put number below previously stored number
      row = prev_row + 1;
      column = prev_column;
    }

    grid[row][column] = i;

    prev_row = row;
    prev_column = column;
  }

  for(row = 0; row < lengths; row++){
    for(column = 0; column < lengths; column++)
      printf("%5d", grid[row][column]);
    printf("\n");
  }

  return 0;
}
