/* This program reverses the words in a sentence. */
#include <stdio.h>

int main(void){
  int i, j, length;

  printf("Maximum number of characters? ");
  scanf("%d", &i);
  char ch = getchar(), sentence[i], term_char;

  printf("Enter a sentence: ");
  for(i = 0, ch = getchar();
  ((ch != '.') && (ch != '?')) && ((ch != '!') && (ch != 10));
  i++, ch = getchar()){
    sentence[i] = ch;
  }
  length = i;
  term_char = ch;

  for(i = length; i >= 0; i--){
    if(sentence[i] == ' ' || i == 0){
      if(i == 0)
        i--;

      for(j = i + 1; (sentence[j] != ' ') && (j <= length); j++)
        printf("%c", sentence[j]);
    
      // don't put a space after the last word
      if(i + 1 != 0)
        putchar(' ');
    }
  }
  printf("%c\n", term_char);

  return 0;
}
