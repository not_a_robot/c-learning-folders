#include <stdio.h>
#include <ctype.h>

int main(void){
  int i, alphabet[26] = {0}, is_anagram;
  char ch;

  printf("Enter first word: ");
  for(i = 0; (ch = getchar()) != '\n'; i++){
    if(isalpha(ch)){
      alphabet[toupper(ch) - 'A']++;
    }
  }

  printf("Enter second word: ");
  for(i = 0; (ch = getchar()) != '\n'; i++){
    if(isalpha(ch)){
      alphabet[toupper(ch) - 'A']--;
    }
  }

  for(i = 0, is_anagram = 1; i < 26; i++){
    if(alphabet[i] != 0){
      is_anagram = 0;
      break;
    }
  }

  if(is_anagram == 1)
    printf("The words are anagrams.\n");
  else
    printf("The words are not anagrams.\n");

  return 0;
}
