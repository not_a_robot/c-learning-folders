#include <stdio.h>

#define STUDENTS 5
#define QUIZZES 5

int main(void){
  int grid[STUDENTS][QUIZZES], i, j, tmp, high, low;

  for(i = 0; i < STUDENTS; i++){
    printf("Enter student %d's %d grades: ", i + 1, QUIZZES);
    for(j = 0; j < QUIZZES; j++)
      scanf("%d", &grid[i][j]);
  }

  printf("\n");
  for(i = 0; i < STUDENTS; i++){
    for(j = 0, tmp = 0; j < QUIZZES; j++){
      tmp += grid[i][j];
    }
    printf("Student %d's total score: %d\n", i + 1, tmp);
    printf("Student %d's average score: %d\n\n", i + 1, tmp / QUIZZES);
  }
  printf("\n");

  for(i = 0; i < QUIZZES; i++){
    for(j = tmp = high = 0, low = grid[0][i]; j < STUDENTS; j++){
      tmp += grid[j][i];
      if(grid[j][i] > high)
        high = grid[j][i];
      else if(grid[j][i] < low)
        low = grid[j][i];
    }
    printf("Average score for quiz %d: %d\n", i + 1, tmp / STUDENTS);
    printf("High score for quiz %d: %d\n", i + 1, high);
    printf("Low score for quiz %d: %d\n\n", i + 1, low);
  }

  return 0;
}
