/* This program computes the value of a word in the SCRABBLE Crossword GAME, by
 * summing the value of all its letters.
 */
#include <stdio.h>
#include <ctype.h>

int main(void){
  int total = 0, values[26] =
  {1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3, 1, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8, 4, 10};
  char ch;

  printf("Enter a word: ");
  while((ch = getchar()) != 10){
    ch = toupper(ch);
    total += values[ch - 65];
  }

  printf("Scrabble value: %d\n", total);

  return 0;
}
