#include <stdio.h>

#define MAX_LENGTH 80

int main(void){
  int shift, i, length;
  char message[MAX_LENGTH], ch;

  printf("Enter message to be encrypted: ");
  for(i = 0; (ch = getchar()) != '\n'; i++)
    message[i] = ch;
  length = i;

  printf("Enter shift amount (1-25): ");
  scanf("%d", &shift);
  if(shift < 0)
    shift += 26;

  printf("Encrypted message: ");
  for(i = 0; i < length; i++){
    if(message[i] >= 'A' && message[i] <= 'Z')
      putchar(((message[i] - 'A') + shift) % 26 + 'A');
    else if(message[i] >= 'a' && message[i] <= 'z')
      putchar(((message[i] - 'a') + shift) % 26 + 'a');
    else
      putchar(message[i]);
  }
  putchar('\n');

  return 0;
}
