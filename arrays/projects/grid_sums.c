#include <stdio.h>

int main(void){
  int grid[5][5], i, j, tmp;

  for(i = 0; i < 5; i++){
    printf("Enter row %d: ", i + 1);
    scanf("%d %d %d %d %d",
      &grid[i][0], &grid[i][1], &grid[i][2], &grid[i][3], &grid[i][4]);
  }

  printf("Row totals: ");
  for(i = 0; i < 5; i++){
    for(j = 0, tmp = 0; j < 5; j++){
      tmp += grid[i][j];
    }
    printf("%d ", tmp);
  }
  printf("\n");

  printf("Column totals: ");
  for(i = 0; i < 5; i++){
    for(j = 0, tmp = 0; j < 5; j++){
      tmp += grid[j][i];
    }
    printf("%d ", tmp);
  }
  printf("\n");

  return 0;
}
