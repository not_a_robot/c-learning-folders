#include <stdio.h>
#include <ctype.h>

int main(void){
  int i, j;

  printf("Enter max number of characters: ");
  scanf("%d", &i);
  char message[i];

  getchar(); // catch stray Enter key

  printf("Enter a message: ");
  for(i = 0; (message[i] = getchar()) != 10; i++)
    ;

  printf("In B1FF-speak: ");
  for(j = 0; j < i; j++){
    switch(toupper(message[j])){
      case 'A':
        printf("4");
        break;
      case 'B':
        printf("8");
        break;
      case 'E':
        printf("3");
        break;
      case 'I':
        printf("1");
        break;
      case 'O':
        printf("0");
        break;
      case 'S':
        printf("5");
        break;
      default:
        printf("%c", toupper(message[j]));
        break;
    }
  }

  for(i = 0; i <= 10; i++)
    printf("!");
  printf("\n");

  return 0;
}
