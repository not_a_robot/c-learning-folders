#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define GRID_LENGTH 10
#define GRID_HEIGHT 10

int main(void){
  char grid[GRID_LENGTH][GRID_HEIGHT], ch;
  int x, y, tmp, success; // x and y are the axises of the grid

  // fill grid with '.'
  for(x = 0; x < GRID_LENGTH; x++){
    for(y = 0; y < GRID_HEIGHT; y++){
      grid[y][x] = '.';
    }
  }

  srand((unsigned) time(NULL));

  for(ch = 'A' - 1, x = y = 0, success = 1; ch < 'Z';){
    if(success == 1){
      ch++;
      grid[y][x] = ch;
    }
    tmp = rand() % 4;

    // break out of the loop if four directions are blocked
    if((grid[y + 1][x] != '.' && grid[y - 1][x] != '.') &&
        (grid[y][x + 1] != '.' && grid[y][x - 1] != '.'))
      break;

    while(tmp == 0){ // 0 moves up
      if(y == 0 || grid[y - 1][x] != '.'){
        success = 0;
        tmp = rand() % 4;
        continue;
      }
      else{
        success = 1;
        y--;
        break;
      }
    }

    while(tmp == 1){ // 1 moves right
      if(x == 9 || grid[y][x + 1] != '.'){
        success = 0;
        tmp = rand() % 4;
        continue;
      }
      else{
        success = 1;
        x++;
        break;
      }
    }

    while(tmp == 2){ // 2 moves down
      if(y == 9 || grid[y + 1][x] != '.'){
        success = 0;
        tmp = rand() % 4;
        continue;
      }
      else{
        success = 1;
        y++;
        break;
      }
    }

    while(tmp == 3){ // 3 moves left
      if(x == 0 || grid[y][x - 1] != '.'){
        success = 0;
        tmp = rand() % 4;
        continue;
      }
      else{
        success = 1;
        x--;
        break;
      }
    }
  }

  for(x = 0; x < GRID_LENGTH; x++){
    for(y = 0; y < GRID_HEIGHT; y++){
      printf("%c ", grid[x][y]);
    }
    printf("\n");
  }

  return 0;
}
