/* This program asks the user to enter a time and display the departure
 * time of a plane from a table from the exercise. The time is converted
 * to minutes only, so it becomes easier to use math on it.
 */
#include <stdio.h>
#include <math.h>

int main(void){
  // the arrival/departure times are stored as total mins from midnight
  int hours, mins, total, i, tmp,
  departure[8] = {480, 583, 679, 767, 840, 945, 1140, 1305},
  arrival[8] = {616, 712, 811, 900, 968, 1075, 1280, 1438};

  printf("Enter a 24-hour time: ");
  scanf("%d:%d", &hours, &mins);
  total = hours * 60 + mins;

  for(tmp = i = 0; i < 8; i++){
    if(fabs(total - departure[i]) < fabs(total - departure[tmp]))
      tmp = i;
  }

  printf("Closest departure time is ");
  if(departure[tmp] / 60 < 13)
    printf("%.2d:%.2d a.m., arriving at ", departure[tmp] / 60, departure[tmp] % 60);
  else
    printf("%.2d:%.2d p.m., arriving at ", departure[tmp] / 60 - 12,
    departure[tmp] % 60);

  if(arrival[tmp] / 60 < 13)
    printf("%.2d:%.2d a.m., arriving at ", arrival[tmp] / 60, arrival[tmp] % 60);
  else
    printf("%.2d:%.2d p.m., arriving at ", arrival[tmp] / 60 - 12,
    arrival[tmp] % 60);

  return 0;
}