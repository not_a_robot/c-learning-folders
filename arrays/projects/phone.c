/* This program translates alphabetic phone numbers to numeric form, leaving
 * any non-alphabetic characters unchanged. I may assume all letters entered
 * are upper case.
 */
#include <stdio.h>

int main(void){
  int i, length = 15;
  char ch[15];

  printf("Enter a phone number: ");
  for(i = 0; (i < length) & ((ch[i] = getchar()) != 10); i++)
      ;
  length = i;

  for(i = 0; i < length; i++){
    if(ch[i] <= 67 && ch[i] >= 65)
      ch[i] = '2';
    else if(ch[i] <= 70 && ch[i] >= 68)
      ch[i] = '3';
    else if(ch[i] <= 73 && ch[i] >= 71)
      ch[i] = '4';
    else if(ch[i] <= 76 && ch[i] >= 74)
      ch[i] = '5';
    else if(ch[i] <= 79 && ch[i] >= 77)
      ch[i] = '6';
    else if(ch[i] <= 82 && ch[i] >= 80)
      ch[i] = '7';
    else if(ch[i] <= 85 && ch[i] >= 83)
      ch[i] = '8';
    else if(ch[i] <= 88 && ch[i] >= 86)
      ch[i] = '9';
  }

  printf("In numeric form: ");
  for(i = 0; i < length; i++){
    printf("%c", ch[i]);
  }
  printf("\n");

  return 0;
}
