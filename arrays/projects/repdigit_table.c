#include <stdio.h>

int main(void){
  int digit_count[10] = {0};
  int digit, i;
  long n;

  printf("Enter a number: ");
  scanf("%ld", &n);

  while(n > 0){
    digit = n % 10;
    digit_count[digit] += 1; // counts how many times digit was seen
    n /= 10;
  }

  printf("Digit: %8d", 0);
  for(i = 1; i < 10; i++)
    printf("%3d", i);

  printf("\nOccurences: ");
  for(i = 0; i < 10; i++){
    printf("%3d", digit_count[i]);
  }
  printf("\n");

  return 0;
}
