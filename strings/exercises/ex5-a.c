#include <stdio.h>
#include <ctype.h>

#define STR_LEN 50

void capitalize(char str[], int n);

int 
main(void){
  char str[STR_LEN];

  printf("Enter a string: ");
  fgets(str, STR_LEN, stdin);
  capitalize(str, STR_LEN);
  printf("Capitalized: %s\n", str);

  return 0;
}

void 
capitalize(char str[], int n){
  int i;
  for(i = 0; i < n && str[i] != '\0'; i++)
    str[i] = toupper(str[i]);
}
