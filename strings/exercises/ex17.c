#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>

#define NAME_LEN 64
#define EXT_LEN 8

bool test_extension(const char *file_name, const char *extension);

int
main(void){
  char file_name[NAME_LEN], extension[EXT_LEN];

  printf("Enter a file name: ");
  fgets(file_name, NAME_LEN, stdin);
  if(file_name[strlen(file_name)] == '\n')
    file_name[strlen(file_name)] = '\0';

  printf("Enter its extension: ");
  fgets(extension, EXT_LEN, stdin);
  if(extension[strlen(extension)] == '\n')
    extension[strlen(extension)] = '\0';
  
  printf("Extension is %s\n",
    test_extension(file_name, extension) ? "correct" : "incorrect");
}

bool
test_extension(const char *file_name, const char *extension){
  while(*file_name++ != '.' && *file_name)
    ;
  while(*file_name && *extension)
    if(toupper(*file_name++) != toupper(*extension++))
      return false;
  return true;
}
