#include <stdio.h>
#include <ctype.h>

int read_line(char str[], int n);

int main(void){
  int n = 50;
  char str[n];

  printf("Enter a string: ");
  read_line(str, n);
  printf("String: %s\n", str);

  return 0;
}

int read_line(char str[], int n){
  int ch, i = 0;

  while(isspace(ch = getchar()))
    ;

  do{
    if(i < n)
      str[i++] = ch;
  }while(!isspace(ch = getchar()) && i < n - 1);
  if(ch == '\n' && i < n - 1)
    str[i++] = '\n';
  str[i] = '\0';
  return i;
}
