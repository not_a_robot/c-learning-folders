#include <stdio.h>
#include <string.h>

#define URL_LEN 64

void remove_filename(char *url);

int
main(void){
  char url[URL_LEN];

  printf("Enter a URL: ");
  fgets(url, URL_LEN, stdin);
  if(url[strlen(url)] == '\n')
    url[strlen(url)] = '\0';
  
  remove_filename(url);
  printf("New url %s\n", url);

  return 0;
}

void
remove_filename(char *url){
  url += strlen(url);
  while(*url != '/')
    url--;
  *url = '\0';
}
