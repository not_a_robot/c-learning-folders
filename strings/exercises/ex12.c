#include <stdio.h>
#include <string.h>

#define STR_LEN 64
#define EXT_LEN 4

void get_extension(const char *file_name, char *extension);

int
main(void){
  char file_name[STR_LEN], extension[EXT_LEN];

  printf("Enter a file name: ");
  fgets(file_name, STR_LEN, stdin);
  get_extension(file_name, extension);

  printf("Extension: %s\n", extension);

  return 0;
}

void
get_extension(const char *file_name, char *extension){
  // don't recalculate value of ends
  const char *file_end = file_name + strlen(file_name) - 1,
    *ext_end = extension + EXT_LEN - 1;

  while(*file_name++ != '.' && file_name < file_end)
    ;
  while(extension < ext_end && file_name < file_end)
    *extension++ = *file_name++;
  *extension = '\0';
}
