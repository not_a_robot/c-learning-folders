b) The " quotation marks mean that "\n" is a string, not a character. Format
specifier "%c" expects a character.
c) The ' quotation marks mean that '\n' is a character, not a string. Format
specifier "%s" expects a string.
e) printf expects a string, '\n' is a character.
h) putchar expects a character, "\n" is a string.
i) puts expects a string, '\n' is a character.
j) prints 2 newline characters, since puts always prints a newline when done.
