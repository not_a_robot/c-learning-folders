#include <stdio.h>

#define STR_LEN 64

int my_strcmp(char *s, char *t);

int
main(void){
  char s[STR_LEN], t[STR_LEN];

  printf("Enter a string: ");
  fgets(s, STR_LEN, stdin);
  printf("Enter second string to compare: ");
  fgets(t, STR_LEN, stdin);

  printf("Output of strcmp: %d\n", my_strcmp(s, t));

  return 0;
}

int 
my_strcmp(char *s, char *t){
  char *p1, *p2;

  for(p1 = s, p2 = t; *p1 == *p2; p1++, p2++)
    if(*p1 == '\0')
      return 0;
  return *p1 - *p2;
}
