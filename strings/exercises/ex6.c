#include <stdio.h>

#define STR_LEN 50

void censor(char *str, int n);

int
main(void){
  char str[STR_LEN];

  printf("Enter a string: ");
  fgets(str, STR_LEN, stdin);
  censor(str, STR_LEN);
  printf("Censored: %s\n", str);

  return 0;
}

void
censor(char *str, int n){
  int i;
  for(i = 0; i < n; i++)
    if(str[i] == 'f' && str[i+1] == 'o' && str[i+2] == 'o'){
      str[i] = str[i+1] = str[i+2] = 'x';
      i += 2;
    }
}
