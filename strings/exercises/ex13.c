#include <stdio.h>
#include <string.h>

#define DOM_LEN 32
#define URL_LEN 64

void build_index_url(const char *domain, char *index_url);

int
main(void){
  char domain[DOM_LEN], index_url[URL_LEN];

  printf("Enter a domain: ");
  fgets(domain, DOM_LEN, stdin);
  // remove newline
  domain[strlen(domain)-1] == '\n' ? domain[strlen(domain)-1] = '\0' : 0;
  build_index_url(domain, index_url);
  
  printf("Index URL: %s\n", index_url);

  return 0;
}

void
build_index_url(const char *domain, char *index_url){
  strcpy(index_url, "http://www.");
  strcat(index_url, domain);
  strcat(index_url, "/index.html");
}
