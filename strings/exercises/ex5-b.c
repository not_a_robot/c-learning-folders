#include <stdio.h>
#include <ctype.h>

#define STR_LEN 50

void capitalize(char *str, int n);

int 
main(void){
  char str[STR_LEN];

  printf("Enter a string: ");
  fgets(str, STR_LEN, stdin);
  capitalize(str, STR_LEN);
  printf("Capitalized: %s\n", str);

  return 0;
}

void 
capitalize(char *str, int n){
  char *p;
  for(p = str; p < str + n && *p != '\0'; p++)
    *p = toupper(*p);
}
