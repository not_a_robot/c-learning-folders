/*
 * This program calculates the average word length for a sentence to one
 * decimal place; for simplicity's sake, punctuation is part of the word it's
 * attached to.
 */
#include <stdio.h>
#include <string.h>

#define SNTC_LEN 64

double compute_average_word_length(const char *sentence);

int
main(void){
  int last;
  char sentence[SNTC_LEN];

  printf("Enter a sentence: ");
  fgets(sentence, SNTC_LEN, stdin);
  last = strlen(sentence) - 1;
  if(sentence[last] == '\n') sentence[last] = '\0';

  printf("Average word length %.1lf\n", compute_average_word_length(sentence));

  return 0;
}

double
compute_average_word_length(const char *sentence){
  int letters = 0, words = 0, last = strlen(sentence) - 1;

  // last word not counted if it isn't followed by a space.
  if(sentence[last] != ' ') words++;

  for(; *sentence != '\0'; sentence++)
    *sentence == ' ' ? words++ : letters++;

  return (double) letters / words;
}
