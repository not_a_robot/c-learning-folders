#include <stdio.h>
#include <string.h>

#define MSG_LENGTH 100

void reverse(char *message);

int
main(void){
  int last;
  char message[MSG_LENGTH];

  printf("Enter a message: ");
  fgets(message, MSG_LENGTH, stdin);
  last = strlen(message) - 1;
  if(message[last] == '\n') message[last] = '\0';
  
  reverse(message);

  printf("Reversal is: %s\n", message);

  return 0;
}

void
reverse(char *message){
  char duplicate[strlen(message)], *p;
  
  strcpy(duplicate, message);
  p = duplicate + strlen(duplicate) - 1;
  while(p >= duplicate)
    *message++ = *p--;
}
