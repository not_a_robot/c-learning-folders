/*
 * rpn.c: this is a calculator that can be used with Reverse Polish Notation
 * (RPN), where operators are placed after their operands instead of inbetween.
 */
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define STACK_SIZE 100
#define EXPR_LEN 100

void make_empty(void);
bool is_empty(void);
bool is_full(void);
void push(int i);
int pop(void);
void stack_overflow(void);
void stack_underflow(void);

int evaluate_RPN_expression(const char *expression);

int stack[STACK_SIZE], top = 0;

int
main(void){
  char expression[EXPR_LEN+1];
  int last;

  while(true){
    printf("Enter an RPN expression: ");
    fgets(expression, STACK_SIZE, stdin);
    last = strlen(expression) - 1;
    if(expression[last] == '\n') expression[last] = '\0';

    printf("Value of expression: %d\n", evaluate_RPN_expression(expression));
  }

  return 0;
}

void
make_empty(void){
  top = 0;
}

bool
is_empty(void){
  return top == 0;
}

bool
is_full(void){
  return top == STACK_SIZE;
}

void
push(int i){
  if(is_full())
    stack_overflow();
  else
    stack[top++] = i;
}

int
pop(void){
  if(is_empty())
    stack_underflow();
  else
    return stack[--top];
}

void
stack_overflow(void){
  printf("Expression is too complex\n");
  exit(74);
}

void
stack_underflow(void){
  printf("Not enough operands in expression\n");
  exit(64);
}

int
evaluate_RPN_expression(const char *expression){
  int arg1, arg2;

  for(; *expression != '='; expression++){
    if(isdigit(*expression))
      push(*expression - '0');
    else{
      switch(*expression){
        case ' ': break;
        case '+':
          push(pop() + pop());
          break;
        case '-':
          arg2 = pop();
          arg1 = pop();
          push(arg1 - arg2);
          break;
        case '*':
          push(pop() * pop());
          break;
        case '/':
          arg2 = pop();
          arg1 = pop();
          push(arg1 / arg2);
          break;
        case '\0':
          stack_underflow();
        default:
          exit(0);
      }
    }
  }
  return pop();
}
