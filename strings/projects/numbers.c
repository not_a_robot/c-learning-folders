/* This program prompts the user for a two-digit number and outputs the
 * English word for it.
 */
#include <stdio.h>

int main(void){
  char *teens[] = {"ten", "eleven", "twelve", "thirteen", "forteen", "fifteen",
    "sixteen", "seventeen", "eighteen", "nineteen"};
  char *ten[] = {"twenty", "thirty", "forty", "fifty", "sixty", "seventy",
    "eighty", "ninety"};
  char *one[] = {"", "one", "two", "three", "four", "five", "six", "seven",
    "eight", "nine"};
  int input, tens, ones;

  printf("Enter a two-digit number: ");
  scanf("%d", &input);
  tens = input / 10;
  ones = input % 10;

  if(input < 20 && input > 9)
    printf("You entered the number %s\n", teens[input-10]);
  else{
    printf("You entered the number ");
    if(tens >= 2)
      printf("%s", ten[tens-2]);
    if(ones)
      printf("-%s", one[ones]);
    putchar('\n');
  }

  return 0;
}
