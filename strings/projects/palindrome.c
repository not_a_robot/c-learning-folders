#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>

#define MSG_LEN 100

bool is_palindrome(const char *message);

int
main(void){
  char message[MSG_LEN];
  int last;

  printf("Enter a message: ");
  fgets(message, MSG_LEN, stdin);
  last = strlen(message) - 1;
  if(message[last] == '\n') message[last] = '\0';

  printf("%s\n", is_palindrome(message) ? "Palindrome" : "Not a palindrome");

  return 0;
}

bool
is_palindrome(const char *message){
  const char *forwards = message, *backwards = message + strlen(message) - 1;
  while(*forwards != '\0'){
    if(!isalpha(*backwards))
      backwards--; // skip non-alphabet characters backward
    else if(isalpha(*forwards)){
      if(toupper(*forwards) != toupper(*backwards))
        return false;
      forwards++;
      backwards--;
    }
    else
      forwards++; // skip non-alphabet characters forward
  }
  return true;
}
