#include <stdio.h>
#include <stdlib.h>

int
main(int argc, char *argv[]){
  int total;
  for(total = atoi(argv[--argc]); argc > 0; total += atoi(argv[--argc]));
  printf("Total: %d\n", total);

  return 0;
}
