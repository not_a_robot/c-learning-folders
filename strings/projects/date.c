#include <stdio.h>

int
main(void){
  int month, day, year;
  const char *month_names[] = {"January", "February", "March", "April", "May",
    "June", "July", "August", "September", "October", "November", "December"};

  printf("Enter a date (mm/dd/yyyy): ");
  scanf("%2d / %2d / %4d", &month, &day, &year);

  printf("%s %d, %d\n", month_names[--month], day, year);

  return 0;
}
