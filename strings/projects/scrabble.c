/*
 * This program computes the value of a word in the SCRABBLE Crossword GAME, by
 * summing the value of all its letters.
 */
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define WORD_LEN 64

int compute_scrabble_value(const char *word);

int main(void){
  char word[WORD_LEN];

  printf("Enter a word: ");
  fgets(word, WORD_LEN, stdin);
  if(word[strlen(word)-1] == '\n') word[strlen(word)-1] = '\0';
  
  printf("Scrabble value: %d\n", compute_scrabble_value(word));

  return 0;
}

int
compute_scrabble_value(const char *word){
  int value = 0;
  for(; *word != '\0'; word++){
    switch(toupper(*word)){
      case 'D': case 'G':
        value += 2; break;
      case 'B': case 'C': case 'M': case 'P':
        value += 3; break;
      case 'F': case 'H': case 'V': case 'W': case 'Y':
        value += 4; break;
      case 'K':
        value += 5; break;
      case 'J': case 'X':
        value += 8; break;
      case 'Q': case 'Z':
        value += 10; break;
      default:
        value++; break;
    }
  }

  return value;
}
