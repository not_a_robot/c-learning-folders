#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>

#define WORD_LEN 32

bool are_anagrams(const char *word1, const char *word2);

int
main(void){
  int last;
  char word1[WORD_LEN], word2[WORD_LEN];

  printf("Enter first word: ");
  fgets(word1, WORD_LEN, stdin);
  last = strlen(word1) - 1;
  if(word1[last] == '\n') word1[last] = '\0';

  printf("Enter second word: ");
  fgets(word2, WORD_LEN, stdin);
  last = strlen(word2) - 1;
  if(word2[last] == '\n') word2[last] = '\0';

  printf("The words %s anagrams.\n",
    are_anagrams(word1, word2) ? "are" : "are not");

  return 0;
}

bool
are_anagrams(const char *word1, const char *word2){
  int alphabet[26] = {0}, i;

  for(; *word1 != '\0'; word1++)
    if(isalpha(*word1))
      alphabet[toupper(*word1) - 'A']++;

  for(; *word2 != '\0'; word2++)
    if(isalpha(*word2))
      alphabet[toupper(*word2) - 'A']--;

  for(i = 0; i < 26; i++)
    if(alphabet[i] != 0)
      return false;
  return true;
}
