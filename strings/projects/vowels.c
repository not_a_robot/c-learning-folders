/* This program counts the number of vowels in a sentence */
#include <stdio.h>
#include <ctype.h>
#include <string.h>

#define SNTC_LEN 64

int compute_vowel_count(const char *sentence);

int
main(void){
  char sentence[SNTC_LEN];

  printf("Enter a sentence: ");
  fgets(sentence, SNTC_LEN, stdin);

  printf("Your sentence contains %d vowels.\n", compute_vowel_count(sentence));

  return 0;
}

int
compute_vowel_count(const char *sentence){
  int vowels = 0;
  for(; *sentence != '\0'; sentence++)
    switch(toupper(*sentence)){
      case 'A': case 'E': case 'I': case 'O': case 'U':
        vowels++;
        break;
    }

  return vowels;
}
