/*
 * This program takes a first and last name, then displays the first name, a
 * comma, and the first initial, followed by a period.
 */
#include <stdio.h>
#include <string.h>

#define NAME_LEN 32

void reverse_name(char *name);

int
main(void){
  int last;
  char name[NAME_LEN];

  printf("Enter a first and last name: ");
  fgets(name, NAME_LEN, stdin);
  last = strlen(name) - 1;
  if(name[last] == '\n') name[last] = '\0';

  reverse_name(name);

  printf("%s\n", name);

  return 0;
}

void
reverse_name(char *name){
  char *p = name, initial;

  for(; *p == ' '; p++) // skip spaces before first name
    ;
  initial = *p;

  while(*p++ != ' ') // skip first name
    ;
  for(; *p == ' '; p++) // skip space between first and last name
    ;

  char *p2 = p;
  for(; *p != ' ' && *p != '\0'; p++) // find where the end of the last name is
    ;
  char last_name[p-p2], *p3 = last_name;

  while(p2 <= p)
    *p3++ = *p2++;

  sprintf(name, "%s, %c.", last_name, initial);
}
