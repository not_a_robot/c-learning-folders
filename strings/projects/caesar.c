#include <stdio.h>
#include <ctype.h>
#include <string.h>

#define MAX_LENGTH 80

void encrypt(char *message, int shift);

int
main(void){
  int shift, last;
  char message[MAX_LENGTH];

  printf("Enter message to be encrypted: ");
  fgets(message, MAX_LENGTH, stdin);
  last = strlen(message) - 1;
  if(message[last] == '\n') message[last] = '\0';

  printf("Enter shift amount (1-25): ");
  scanf("%d", &shift);
  if(shift < 0)
    shift += 26;

  encrypt(message, shift);

  printf("Encrypted message: %s\n", message);

  return 0;
}

void
encrypt(char *message, int shift){
  int i;

  for(i = 0; i < (int) strlen(message); i++){
    if(isupper(message[i]))
      message[i] = (message[i] - 'A' + shift) % 26 + 'A';
    else if(islower(message[i]))
      message[i] = (message[i] - 'a' + shift) % 26 + 'a';
  }
}
