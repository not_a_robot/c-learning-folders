#include <stdio.h>
#include <string.h>

#define WORD_LEN 20
#define TERM_LEN 4 // word length to terminate at

char *get_string(char *str, int size);

int
main(void){
  char word[WORD_LEN+1], smallest_word[WORD_LEN+1] = "\0", 
    largest_word[WORD_LEN+1] = "\0";

  printf("Enter a word: ");
  get_string(word, WORD_LEN);
  strcpy(smallest_word, word);
  strcpy(largest_word, word);

  printf("Enter a word: ");
  while(strlen(get_string(word, WORD_LEN)) != TERM_LEN){
    if(strcmp(word, largest_word) > 0)
      strcpy(largest_word, word);
    else if(strcmp(word, smallest_word) < 0)
      strcpy(smallest_word, word);
    printf("Enter a word: ");
  }

  printf("\nSmallest word: %s\n", smallest_word);
  printf("Largest word: %s\n", largest_word);

  return 0;
}

// fgets from stdin, removing newline
char *
get_string(char *str, int size){
  int last;

  fgets(str, size, stdin);
  last = strlen(str) - 1;
  if(str[last] == '\n')
    str[last] = '\0';
  return str;
}
