/* This program counts the number of vowels in a sentence */
#include <stdio.h>
#include <ctype.h>

int main(void){
  int vowels = 0;
  char ch;

  printf("Enter a sentence: ");
  while((ch = getchar()) != 10){
    ch = toupper(ch);
    switch(ch){
      case 'A': case 'E': case 'I': case 'O': case 'U':
        vowels++;
        break;
    }
  }

  printf("Your sentence contains %d vowels.\n", vowels);

  return 0;
}
