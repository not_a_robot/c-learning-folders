/* This program takes a first and last name, then displays the first name, a
 * comma, and the first initial, followed by a period.
 */
#include <stdio.h>

int main(void){
  char initial, ch;

  printf("Enter a first and last name: ");
  while((initial = getchar()) == ' ')  // only set initial when it isn't space
    ;

  while(getchar() != ' ') // ignore the rest of the first name
    ;
  // can't have something like:
  // while((ch = getchar()) != 10 & (ch = getchar()) != ' ')
  // since ch will get set twice
  for(ch = getchar(); (ch != 10) & (ch != ' '); ch = getchar())
    putchar(ch);

  printf(", %c.\n", initial);

  return 0;
}
