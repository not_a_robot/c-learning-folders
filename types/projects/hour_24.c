#include <stdio.h>
#include <ctype.h>

int main(void){
  int hours, mins;
  char am_pm;

  printf("Enter a 12-hour time: ");
  scanf("%d:%d %c", &hours, &mins, &am_pm);
  am_pm = toupper(am_pm);
  if(am_pm == 'P')
    hours += 12;
  printf("Equivalent 24-hour time: %d:%d\n", hours, mins);

  return 0;
}
