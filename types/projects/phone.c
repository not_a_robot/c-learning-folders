/* This program translates alphabetic phone numbers to numeric form, leaving
 * any non-alphabetic characters unchanged. I may assume all letters entered
 * are upper case.
 */
#include <stdio.h>

int main(void){
  char ch;

  printf("Enter a phone number: ");
  while((ch = getchar()) != 10){  // exit when Enter is hit
    if(ch <= 67 && ch >= 65)
      printf("2");
    else if(ch <= 70 && ch >= 68)
      printf("3");
    else if(ch <= 73 && ch >= 71)
      printf("4");
    else if(ch <= 76 && ch >= 74)
      printf("5");
    else if(ch <= 79 && ch >= 77)
      printf("6");
    else if(ch <= 82 && ch >= 80)
      printf("7");
    else if(ch <= 85 && ch >= 83)
      printf("8");
    else if(ch <= 88 && ch >= 86)
      printf("9");
    else
      printf("%c", ch);
  }
  printf("\n");

  return 0;
}
