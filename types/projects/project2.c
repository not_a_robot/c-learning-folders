/* This program prints a table of squares. When 24 entries have been printed,
 * the user is prompted to continue by pressing Enter.
 */
#include <stdio.h>

int main(void){
  int n, i, j;  // j to keep count of when 24 squares have been printed

  printf("This program prints a table of squares.\n");
  printf("Enter number of entries in table: ");
  scanf("%d", &n);
  // scanf leaves behind any characters that aren't consumed when writing to n,
  // including the newline character. This getchar() call prevents the next
  // getchar() call from reading that newline character, preventing a huge
  // mess.
  getchar();

  for(i = 1, j = 0; i <= n; i++, j++){
    if(j == 24){
      printf("Press Enter to continue...");
      while(getchar() != 10)
        ;
      j = 0;
    }
    printf("%10d%10d\n", i, i * i);
  }

  return 0;
}
