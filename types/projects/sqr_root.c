/* This program uses Newton's method to compute the square root of a positive
 * floating-point number.
 */
#include <stdio.h>
#include <math.h>

int main(void){
  // z = old value of y, to terminate when the difference between the old value
  // of y and the new value of y is less than .00001 * y.
  double x, y, z;

  printf("Enter a positive number: ");
  scanf("%lf", &x);
  x = fabs(x); // make sure x is positive

  y = 1;
  do{
    z = y;
    y = (y + x / y) / 2;
  } while(fabs(z - y) >= (.00001 * y));

  printf("Square root: %lf\n", y);

  return 0;
}
