/* This program evaluates an expression of floats, but with no operator
 * precedence.
 */
#include <stdio.h>

int main(void){
  float value, tmp;
  char sign;

  printf("Enter an expression: ");

  // first value is initial value, exit upon Enter
  for(scanf("%f", &value); (sign = getchar()) != 10;){
    // can't put this after the second semicolon above, as it'll run at the end
    scanf("%f", &tmp);

    switch(sign){
      case '+':
        value += tmp;
        break;
      case '-':
        value -= tmp;
        break;
      case '*':
        value *= tmp;
        break;
      case '/':
        value /= tmp;
        break;
      default:
        printf("INVALID SIGN\n");
        break;
    }
  }

  printf("Value of expression: %g\n", value);

  return 0;
}
