46341 is the minimum value of `n` that causes an error on `int` types.
32767 is the minimum value of `n` that causes an error on `short` types.
10000000 as a value for `n` still does not cause an error on `long` types, but it takes a lot of time to calculate the squares in order and a lot of cpu time too... it's safe to say that `long` is stored in 64-bits...

From these observations, I can tell that `int` is a 32-bit number, `short` is also 32-bits (?), and `long` is 64-bits.
