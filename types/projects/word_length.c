/* This program calculates the average word length for a sentence to one
 * decimal place; for simplicity's sake, punctuation is part of the word it's
 * attached to.
 */
#include <stdio.h>

int main(void){
  float words = 1, letters = 0;
  char ch;

  printf("Enter a sentence: ");

  while((ch = getchar()) != 10){
    if(ch == ' '){
      words++;
      continue;
    }
    else
      letters++;
  }

  printf("Average word length: %.1f\n", letters/words);

  return 0;
}
