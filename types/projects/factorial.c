/* This program computes the factorial of a positive integer.
 * Using a short variable to store the factorial means the largest value for n
 * that correctly prints its factorial is 7.
 * The largest value for an int is 12.
 * The largest value for a long is 20.
 * The largest value for a long long is 20.
 * The largest value for a float is 13.
 * The largest value for a double is 22.
 * The largest value for a long double is 25.
 */
#include <stdio.h>

int main(void){
  int n, i;
  long double factorial;

  printf("Enter a positive integer: ");
  scanf("%d", &n);

  for(factorial = i = 1; i <= n; i++){
    factorial *= i;
  }

  printf("Factorial of %d: %Lf\n", n, factorial);

  return 0;
}
