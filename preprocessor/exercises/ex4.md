a) `AVG(1+2,3+4)` becomes `(1+2-3+4)/2`. Fixed by adding more brackets:
`#define AVG(x,y) (((x)-(y))/2)`
b) `5/AREA(3,2)` becomes `5/(3)*(2)`. Fixed by adding more brackets: `#define
AREA(x,y) ((x) * (y))`
