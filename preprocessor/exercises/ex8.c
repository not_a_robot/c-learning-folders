#include <stdio.h>

#define STRINGSIZE(x) #x
#define STRINGSIZE2(x) STRINGSIZE(x)

#define LINE_FILE "Line " STRINGSIZE2(__LINE__) " of file " __FILE__

int
main(void){
	const char *str = LINE_FILE;
	printf("%s\n", str);

	return 0;
}
