a)
```
long long_max(long x, long y)
{
	return x > y ? x : y;
}
```
b) When the string "unsigned long" is "pasted" to "_max", it expands to
"unsigned long_max". Function prototypes/declarations aren't allowed to have
spaces in them.
c) Make a type definition for "unsigned long" to a type with no whitespaces
i.e. `typedef unsigned long UNSIGNED_LONG`.
