#include <stdio.h>

#define CHECK(x,y,n) ((x) < (n) && (x) >= 0 && (y) < (n) && (y) >= 0 ? 1 : 0)
#define MEDIAN(x,y,z)\
  (\
    (x) <= (y) ?\
      (y) <= (z) ? (y) :\
        (x) <= (z) ? (z) : (x)\
    : (z) <= (y) ? y : (x) <= (z) ? (x) : (z)\
  )
// #define MEDIAN(x,y,z) ((x) <= (y) ? (y) <= (z) ? (y) : (x) <= (z) ? z : x : (z) <= (y) ? y : (x) <= (z) ? (x) : (z)) // lol
#define POLYNOMIAL(x) (3*((x) * (x) * (x) * (x) * (x)) + 2*((x) * (x) * (x) * (x))\
  - 5*((x) * (x) * (x)) - ((x) * (x)) + 7 * (x) - 6)

int
main(void){
  int x = 3, y = 1, n = 2;

  printf("CHECK: %d\nMEDIAN: %d\nPOLYNOMIAL: %d\n", CHECK(x, y, n),
    MEDIAN(x, y, n), POLYNOMIAL(x));

  return 0;
}
