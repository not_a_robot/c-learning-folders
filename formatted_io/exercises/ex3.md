a) they are the same; scanf ignores all whitespace
b) they are the same; scanf ignores all whitespace, and the `-` sign cannot come after the first `%d`, so scanf cannot confuse it.
c) they are the same: scanf reads the character and since there are no format specifiers it is terminated.
d) once again; scanf ignores whitespace (but it will require a comma to be there)
