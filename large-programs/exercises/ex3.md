No problem at all as long as no local file has the same name as a system
header, as quotation marks mean to search locally first and then search system
wide header files.
