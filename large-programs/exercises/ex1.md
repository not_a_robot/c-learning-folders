a)
- Easier to manage and deal with shorter line chuncks of closely related code
than one large structure.
- Easier to debug and find where a particular error is in a program.
b)
- Can introduce unnecessary complexity trying to divide a program (in smaller
programs particularly).
