a)
```
Output if DEBUG is defined:
Value of i: 1
Value of j: 2
Value of k: 3
Value of i + j: 3
Value of 2 * i + j - k: 1
```

b)
```
Output if DEBUG is not defined:\n
```

c) `PRINT_DEBUG` is just defined as nothing if `DEBUG` is not defined, so any
place it is written would just be replaced by blank lines by the preprocessor.
d) Yes, as `debug.h` checks if `DEBUG` is defined, so it has to be defined
before the check so it'd show up as so.
