#include <stdio.h>
#include <stdlib.h>

#include "stack.h"

extern int expression[];
extern int top;

void make_empty(void){
  top = 0;
}

bool is_empty(void){
  return top == 0;
}

bool is_full(void){
  return top == STACK_SIZE;
}

void push(int i){
  if(is_full())
    stack_overflow();
  else
    expression[top++] = i;
}

int pop(void){
  if(is_empty())
    stack_underflow();
  else
    return expression[--top];
}

void stack_overflow(void){
  printf("Expression is too complex\n");
  exit(74);
}
void stack_underflow(void){
  printf("Not enough operands in expression\n");
  exit(64);
}
