/* rpn.c: this is a calculator that can be used with Reverse Polish Notation
 * (RPN), where operators are placed after their operands instead of inbetween.
 */
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#include "stack.h"

int expression[STACK_SIZE];
int top = 0;

int main(void){
  char ch;
  int arg1, arg2;

  printf("Enter an RPN expression: ");

  while(true){
    scanf(" %c", &ch);

    if(ch >= '0' && ch <= '9')
      push(ch - '0');
    else{
      switch(ch){
        case '+':
          push(pop() + pop());
          break;
        case '-':
          arg2 = pop();
          arg1 = pop();
          push(arg1 - arg2);
          break;
        case '*':
          push(pop() * pop());
          break;
        case '/':
          arg2 = pop();
          arg1 = pop();
          push(arg1 / arg2);
          break;
        case '=':
          printf("Value of expression: %d\nEnter an RPN expression: ", pop());
          break;
        default:
          exit(0);
      }
    }
  }

  return 0;
}
