#include <stdio.h>

void find_two_largest(int a[], int n, int *largest, int *second_largest);

int main(void){
  int n, first, second, *largest = &first, *second_largest = &second, i;
  printf("Enter array length: ");
  scanf("%d", &n);
  int a[n];

  printf("Enter array elements: ");
  for(i = 0; i < n; i++)
    scanf(" %d", &a[i]);

  find_two_largest(a, n, largest, second_largest);
  printf("Largest: %d\nSecond Largest: %d\n", first, second);

  return 0;
}

void find_two_largest(int a[], int n, int *largest, int *second_largest){
  int i;

  if(a[0] > a[1]){
    *largest = a[0];
    *second_largest = a[1];
  }
  else{
    *largest = a[1];
    *second_largest = a[0];
  }

  *largest = a[0];
  for(i = 1; i < n; i++)
    if(a[i] > *largest){
      *second_largest = *largest;
      *largest = a[i];
    }
}
