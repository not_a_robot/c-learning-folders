#include <stdio.h>

void swap(int *p, int *q);

int main(void){
  int i, j;

  printf("Enter two values: ");
  scanf("%d %d", &i, &j);

  swap(&i, &j);
  printf("Their values swapped: %d %d\n", i, j);

  return 0;
}

void swap(int *p, int *q){
  int tmp = *p;
  *p = *q;
  *q = tmp;
}
