#include <stdio.h>

void split_date(int day_of_year, int year, int *month, int *day);

int main(void){
  int year, day_of_year, month, day, *m = &month, *d = &day;

  printf("Enter year: ");
  scanf("%d", &year);
  printf("Enter day of year: ");
  scanf("%d", &day_of_year);

  split_date(day_of_year, year, m, d);
  printf("%d/%d\n", day, month);

  return 0;
}

void split_date(int day_of_year, int year, int *month, int *day){
  int i, can_continue = 1;

  for(i = 0; i <= 12 && can_continue; i++)
    switch(i){
      case 0:
        if(day_of_year <= 30)
          can_continue = 0;
        else{
          day_of_year -= 31;
          if((i == 1
            && (day_of_year <= 28 || (year % 4 == 0 && day_of_year <= 29)))
            || day_of_year <= 30)
            can_continue = 0;
        }
        break;
      case 2:
        day_of_year -= year % 4 == 0 ? 29 : 28;
        if(day_of_year <= 30)
          can_continue = 0;
        break;
      case 4: case 6: case 9: case 11:
        day_of_year -= 30;
        if(day_of_year <= 31)
          can_continue = 0;
        break;
      default:
        day_of_year -= 31;
        if((i == 1
          && (day_of_year <= 28 || (year % 4 == 0 && day_of_year <= 29)))
          || day_of_year <= 30)
          can_continue = 0;
        break;
    }
  *month = i;
  *day = day_of_year;
}
