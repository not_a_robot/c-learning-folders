#include <stdio.h>

int *find_largest(int a[], int n);

int main(void){
	int n, i;
	printf("Enter array length: ");
	scanf("%d", &n);
	int a[n];

	printf("Enter array elements: ");
	for(i = 0; i < n; i++)
		scanf(" %d", &a[i]);

	printf("Address of largest element: %p\n", find_largest(a, n));

	return 0;
}

int *find_largest(int a[], int n){
	int i, largest = a[0], element = 0;
	for(i = 1; i < n; i++){
		if(a[i] > largest){
			largest = a[i];
			element = i;
		}
	}

	return &a[element];
}
