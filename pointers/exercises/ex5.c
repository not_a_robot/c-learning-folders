#include <stdio.h>

void split_time(long total_sec, int *hr, int *min, int *sec);

int main(void){
  long total_sec;
  int hours, minutes, seconds, *hr = &hours, *min = &minutes, *sec = &seconds;

  printf("Enter seconds since midnight: ");
  scanf("%ld", &total_sec);

  split_time(total_sec, hr, min, sec);
  printf("Hour(s): %d\nMinute(s): %d\nSecond(s):%d\n", hours, minutes, seconds);

  return 0;
}

void split_time(long total_sec, int *hr, int *min, int *sec){
  *min = total_sec / 60;
  *hr = *min / 60;
  *min -= *hr * 60;
  *sec = total_sec % 60;
}
