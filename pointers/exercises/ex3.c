#include <stdio.h>

void avg_sum(double a[], int n, double *avg, double *sum);

int main(void){
  int n, i;
  double average, total, *avg = &average, *sum = &total;
  printf("Enter array length: ");
  scanf("%d", &n);
  double a[n];

  printf("Enter array elements: ");
  for(i = 0; i < n; i++)
    scanf(" %lf", &a[i]);
  avg_sum(a, n, avg, sum);

  printf("Average: %lf\nSum: %lf\n", average, total);

  return 0;
}

void avg_sum(double a[], int n, double *avg, double *sum){
  int i;

  *sum = 0.0;
  for(i = 0; i < n; i++)
    *sum += a[i];
  *avg = *sum / n;
}
