/* This program asks the user to enter a time and display the departure
 * time of a plane from a table from the exercise. The time is converted
 * to minutes only, so it becomes easier to use math on it.
 */
#include <stdio.h>

void find_closest_flight(int desired_time, int *departure_time,
  int *arrival_time);

int main(void){
  int hours, mins, total, departure, arrival, d_hour, d_min, a_hour, a_min;
  char dc, ac;

  printf("Enter a 24-hour time: ");
  scanf("%d:%d", &hours, &mins);
  total = hours * 60 + mins;

  find_closest_flight(total, &departure, &arrival);

  d_hour = departure / 60;
  d_min = departure - d_hour * 60;
  a_hour = arrival / 60;
  a_min = arrival - a_hour * 60;

  if(departure > 720){
    dc = ac = 'p';
    if(d_hour > 12)
      d_hour -= 12;
    if(a_hour > 12)
      a_hour -= 12;
  }
  else if(arrival > 720){
    dc = 'a';
    ac = 'p';
    if(a_hour > 12)
      a_hour -= 12;
  }
  else
    dc = ac = 'a';

  printf("Closest departure time %d:%.2d %c.m., arriving at %d:%.2d %c.m.\n",
    d_hour, d_min, dc, a_hour, a_min, ac);

  return 0;
}

void find_closest_flight(int desired_time, int *departure_time,
  int *arrival_time){
  // the second evaluation is the halfway point exactly between 8:00 and 9:43
  if(desired_time < 480 || desired_time <= 531.5){
    *departure_time = 480;
    *arrival_time = 616;
  }
  else if(desired_time <= 631){ // halfway point between 9:43 and 11:19
    *departure_time = 583;
    *arrival_time = 712;
  }
  else if(desired_time <= 723){ // halfway point between 11:19 and 12:47
    *departure_time = 679;
    *arrival_time = 811;
  }
  else if(desired_time <= 803.5){ // halfway point between 12:47 and 2:00
    *departure_time = 767;
    *arrival_time = 900;
  }
  else if(desired_time <= 892.5){ // halfway point between 2:00 and 3:00
    *departure_time = 840;
    *arrival_time = 968;
  }
  else if(desired_time <= 1042.5){ // halfway point between 3:45 and 7:00
    *departure_time = 945;
    *arrival_time = 1075;
  }
  else if(desired_time <= 1222.5){ // halfway point between 7:00 and 9:45
    *departure_time = 1140;
    *arrival_time = 1075;
  }
  else{
    *departure_time = 1305;
    *arrival_time = 1438;
  }
}
