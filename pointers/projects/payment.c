#include <stdio.h>

void pay_amount(int dollars, int *twenties, int *tens, int *fives, int *ones);

int main(void){
  int dollars, b20, b10, b5, b1;

  printf("Enter a dollar amount: ");
  scanf("%d", &dollars);

  pay_amount(dollars, &b20, &b10, &b5, &b1);
  printf("$20 bills: %d\n$10 bills: %d\n$5 bills: %d\n$1 bills: %d\n",
    b20, b10, b5, b1);

  return 0;
}

void pay_amount(int dollars, int *twenties, int *tens, int *fives, int *ones){
    *twenties = dollars / 20;
    *tens = (dollars - (*twenties * 20)) / 10;
    *fives = (dollars - (*twenties * 20) - (*tens * 10)) / 5;
    *ones = dollars - (*twenties * 20) - (*tens * 10) - (*fives * 5);
}
