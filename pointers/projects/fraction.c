/* This program reduces a fraction to its lowest terms by finding the GCD, and
 * dividing both terms of the fraction by the GCD
 */
#include <stdio.h>

void reduce(int numerator, int denominator, int *reduced_numerator,
  int *reduced_denomerator);

int main(void){
  int frac1, frac2, n_reduc, d_reduc;

  printf("Enter a fraction: ");
  scanf("%d/%d", &frac1, &frac2);

  reduce(frac1, frac2, &n_reduc, &d_reduc);
  printf("In lowest terms: %d/%d\n", n_reduc, d_reduc);

  return 0;
}

void reduce(int numerator, int denomenator, int *reduced_numerator,
  int *reduced_denomerator){
  int remainder;
  *reduced_numerator = numerator;
  *reduced_denomerator = denomenator;

  while(numerator != 0){
    remainder = denomenator % numerator;
    denomenator = numerator;
    numerator = remainder;
  }

  *reduced_numerator /= denomenator;
  *reduced_denomerator /= denomenator;
}
