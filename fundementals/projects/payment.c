#include <stdio.h>

int main(void){
  int dollar;

  printf("Enter a dollar amount: ");
  fflush(stdout);
  scanf("%d", &dollar);

  int b20 = dollar / 20, b10 = (dollar - (b20 * 20)) / 10, b5 = (dollar - (b20 * 20) - (b10 * 10)) / 5, b1 = (dollar - (b20 * 20) - (b10 * 10) - (b5 * 5));


  /*
  printf("$20 bills: %d\n", dollar / 20);
  printf("$10 bills: %d\n", (dollar - (dollar / 20) * 20) / 10);
  */
  printf("$20 bills: %d\n$10 bills: %d\n$5 bills: %d\n$1 bills: %d\n", b20, b10, b5, b1);

  return 0;
}
