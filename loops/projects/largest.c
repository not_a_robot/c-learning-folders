/* This program just finds the largest number inputed by the user */
#include <stdio.h>

int main(void){
  float tmp, largest = 0; // a temporary number to store user input, and a 
                          // number to store the current largest number.
  for(;;){
    printf("Enter a number: ");
    scanf("%f", &tmp);

    if(tmp <= 0)
      break;
    else if(tmp > largest)
      largest = tmp;
  }

  printf("The largest number entered was %g\n", largest);

  return 0;
}
