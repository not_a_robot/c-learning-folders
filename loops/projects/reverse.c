/* This program reverses any whole number, with any number of digits */
#include <stdio.h>

int main(void){
  int num, rev = 0, tmp;

  printf("Enter a number: ");
  scanf("%d", &num);

  do{
    tmp = num % 10; // store ones digits of the number in a temporary variable
    rev = rev * 10 + tmp; // makes the current reverse number move to the \
    left once, and make the ones digit of the number the ones digit of the \
      reversed number
    num /= 10;  // reduce the number by one digit
  }while(num != 0); // stop when the number no longer has any digits
  printf("The reversal is: %d\n", rev);

  return 0;
}
