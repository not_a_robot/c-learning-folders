/* This program prints all the even square numbers between 1 and n */
#include <stdio.h>

int main(void){
  int n;

  printf("Enter a number: ");
  scanf("%d", &n);

  for(int i = 2; i * i <= n; i += 2){
    printf("%d\n", i * i);
  }

  return 0;
}
