/* This program uses Euclid's algorithm to find the greatest common divisor
 * (GCD) of two numbers.
 */
#include <stdio.h>

int main(void){
  int n, m, tmp;

  printf("Enter two integers: ");
  scanf("%d %d", &n, &m);

  while(n != 0){
    tmp = m % n;
    m = n;
    n = tmp;
  }

  printf("Greatest common divisor: %d\n", m);

  return 0;
}
