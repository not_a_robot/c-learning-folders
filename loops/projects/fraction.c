/* This program reduces a fraction to its lowest terms by finding the GCD, and
 * dividing both terms of the fraction by the GCD
 */
#include <stdio.h>

int main(void){
  int frac1, frac2;

  printf("Enter a fraction: ");
  scanf("%d/%d", &frac1, &frac2);

  // get the GCD, adapted from gcd.c
  // ints here to make sure original value of fraction components exists
  int n = frac1, gcd = frac2, remainder; // m renamed gcd, to make clear it is\
  the GCD 
  while(n != 0){
    remainder = gcd % n;
    gcd = n;
    n = remainder;
  }

  printf("In lowest terms: %d/%d\n", frac1 / gcd, frac2 / gcd);

  return 0;
}
