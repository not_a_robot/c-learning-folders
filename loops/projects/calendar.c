/* This program prints a one-month calendar, where the user specifies the
 * number of days in a month and the day of the week the month begins.
 */
#include <stdio.h>

int main(void){
  int days, i, starting;

  printf("Enter number of days in month: ");
  scanf("%d", &days);
  printf("Enter starting day of the week (1=Sun, 7=Sat): ");
  scanf("%d", &starting);

  for(i = starting - 1; i != 0; i--){  // print 3 spaces for every day before \
    first day on first week
    printf("   ");
  }

  // count keeps track of what column number the calendar is on, so it needs \
  to have an offset at first so it doesn't go out of the calendar
  for(int i = 1, count = starting; i <= days; i++, count++){
    printf("%3d", i);
    if(count == 7){ // each week has 7 days, so a newline is printed on the \
      7th day and the counter is reset
      printf("\n");
      count = 0;
    }
  }
  printf("\n");

  return 0;
}
