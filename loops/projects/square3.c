/* Prints a table of squares usinga ann odd method */
#include <stdio.h>

int main(void){
  int n, odd;

  printf("This program prints a table of squares\n");
  printf("Enter number of entries in table: ");
  scanf("%d", &n);

  odd = 3;
  for(int i = 1, square = 1; i <= n; odd += 2){
    printf("%10d%10d\n", i, i * i);
    ++i;
    square += odd;
  }

  return 0;
}
