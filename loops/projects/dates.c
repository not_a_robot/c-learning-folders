/* This program prompts the user to enter as many dates as they want, and find
 * out which is the earliest
 */
#include <stdio.h>

int main(void){
  int day_1, month_1, year_1, day_2, month_2, year_2;

  printf("Enter a date (dd/mm/yy): ");
  scanf("%d/%d/%d", &day_1, &month_1, &year_1);

  for(;;){
    printf("Enter a date (dd/mm/yy): ");
    scanf("%d/%d/%d", &day_2, &month_2, &year_2);

    if(month_2 == 0 || day_2 == 0){ // you can have a 0 year, but no 0 months \
      or days
      break;
    }
    // convert both dates to days to compare them; the earlier one will \
    reside in *_1
    else if((((year_2 * 12) * 30) + (month_2 * 30) + day_2) < \
       (((year_1 * 12) * 30) + (month_1 * 30) + day_1)){
      year_1 = year_2;
      month_1 = month_2;
      day_1 = day_2;
    }
  }

  printf("%.2d/%.2d/%.2d is the earliest date.\n", day_1, month_1, year_1);

  return 0;
}
