/* This program approximates the mathematical constant e. It can be
 * approximated as 
 * e = 1 + 1/1! + 1/2! + 1/3! + ... + 1/n!
 * where n is an integer entered by the user
 */
#include <stdio.h>

int main(void){
  int n, i, j;  // n: inputed by user. i and j: increment-ers.
  float f, e = 1; // f: the factorial in a loop. e: approximation of e

  printf("Enter a number: ");
  scanf("%d", &n);

  for(i = 1; i <= n; i++){
    for(j = 1, f = 1; j <= i; j++){
      f *= j;
    }
    e += 1 / f;
  }

  printf("e is approximately %.32g\n", e);

  return 0;
}
