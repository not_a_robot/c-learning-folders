/* This program approximates the mathematical constant e. It can be
 * approximated as 
 * e = 1 + 1/1! + 1/2! + 1/3! + ... + 1/n!
 * where n is an integer entered by the user
 * This program keeps adding terms until the current term is less than a, where
 * a is a float number entered by the user.
 */
#include <stdio.h>

int main(void){
  int n, i, j;  // n: inputed by user. i and j: increment-ers.
  float a, f, e = 1; // a: inputed by user f: the factorial in a loop.\
  e: approximation of e

  printf("Enter a number: ");
  scanf("%d", &n);
  printf("e should be smaller than what number? ");
  scanf("%f", &a);

  for(i = 1; i <= n; i++){
    for(j = 1, f = 1; j <= i; j++){
      f *= j;
    }
    e += 1 / f;
    if(e < a)
      break;
  }

  printf("e is approximately %.32g\n", e);

  return 0;
}
